
import Foundation
import UIKit
import IQKeyboardManagerSwift
import SwiftyJSON
import SwiftyUserDefaults
import KAPinField

class ForgotVC: BaseVC {

    @IBOutlet weak var edt_email: UITextField!
    var email = ""
                 
    override func viewDidLoad() {
        super.viewDidLoad()
        setEdtPlaceholder(edt_email, placeholderText: "", placeColor: .darkGray, padding: .left(10))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showNavBar()
    }
    
    @IBAction func submitBtnClicked(_ sender: Any) {
        self.email = self.edt_email.text ?? ""
        if email.isEmpty || !email.isValidEmail(){
            self.showToast(Messages.VALID_EMAIL_REQUIRE)
            return
        }else{
            // goto send forgot password request
            self.showLoadingView(vc: self)
            ApiManager.forgotconfirmemail(email: email) { (isSuccess, pincode) in
                self.hideLoadingView()
                if isSuccess{
                    let tovc = self.createVC(VCs.VERIFY) as! VerifyVC
                    tovc.pincode = pincode
                    tovc.email = self.email
                    self.gotoNavPresentWithVC(tovc, fullscreen: true)
                }else{
                    print("neterror")
                }
            }
        }
    }
}


