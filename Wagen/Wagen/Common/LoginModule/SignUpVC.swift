//
//  SignUpVC.swift
//  EveraveUpdate
//
//  Created by Mac on 5/9/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//


import Foundation
import UIKit
import IQKeyboardManagerSwift
import SwiftyJSON
import SwiftyUserDefaults
import Photos
import GDCheckbox
import SKCountryPicker
import GoogleSignIn
import FacebookCore
import FBSDKLoginKit
import AuthenticationServices

class SignUpVC: BaseVC, UITextFieldDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    @IBOutlet weak var edt_first_name: UITextField!
    @IBOutlet weak var edt_last_name: UITextField!
    @IBOutlet weak var edt_email: UITextField!
    @IBOutlet weak var edt_phone_number: UITextField!
    @IBOutlet weak var edt_password: UITextField!
    @IBOutlet weak var edt_confirm_pwd: UITextField!
    
    var first_name = ""
    var last_name = ""
    var email = ""
    var phone_number = ""
    var password = ""
    var confirm_pwd = ""
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showNavBar()
        //self.addleftButton()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        editInit()
    }
    
    func loadLayout() {
//        self.edtrealName.text = "tester"
//        self.edtuserName.text = "tester"
//        self.edtemail.text = "tester@gmail.com"
//        self.edtpassword.text = "123"
//        self.edtconfirmpassword.text = "123"
    }
    
    func editInit() {
        setEdtPlaceholder(edt_first_name, placeholderText: "", placeColor: UIColor.lightGray, padding: .left(10))
        setEdtPlaceholder(edt_last_name, placeholderText: "", placeColor: UIColor.lightGray, padding: .left(10))
        setEdtPlaceholder(edt_email, placeholderText: "", placeColor: UIColor.lightGray, padding: .left(10))
        setEdtPlaceholder(edt_phone_number, placeholderText: "", placeColor: UIColor.lightGray, padding: .left(10))
        setEdtPlaceholder(edt_password, placeholderText: "", placeColor: UIColor.lightGray, padding: .left(10))
        setEdtPlaceholder(edt_confirm_pwd, placeholderText: "", placeColor: UIColor.lightGray, padding: .left(10))
        let titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        UISegmentedControl.appearance().setTitleTextAttributes(titleTextAttributes, for: .selected)
    }
    
    @IBAction func signupBtnClicked(_ sender: Any) {
        first_name = self.edt_first_name.text ?? ""
        last_name = self.edt_last_name.text ?? ""
        email = self.edt_email.text ?? ""
        phone_number = self.edt_phone_number.text ?? ""
        password = self.edt_password.text ?? ""
        confirm_pwd = self.edt_confirm_pwd.text ?? ""
        if first_name.isEmpty{
            self.showToast(Messages.FIRST_NAME_REQUIRE)
            return
        }
        if last_name.isEmpty{
            self.showToast(Messages.LAST_NAME_REQUIRE)
            return
        }
        if email.isEmpty{
            self.showToast(Messages.EMAIL_REQUIRE)
            return
        }
        if !email.isValidEmail(){
            self.showToast(Messages.VALID_EMAIL_REQUIRE)
            return
        }
        if phone_number.isEmpty{
            self.showToast(Messages.PHONE_NUMBER_REQUIRE)
            return
        }
        if password.isEmpty{
            self.showToast(Messages.PASSWORD_REQUIRE)
            return
        }
        if confirm_pwd != password{
            self.showToast(Messages.CONFIRM_PASSWORD_MATCH)
            return
        }
       
        else{
            // goto signup
            self.showLoadingView(vc: self)
            ApiManager.registerUser(first_name: first_name, last_name: last_name, email: email, phonenumber: phone_number, password: password, photo: "", account_type: AccountType.GENERAL, social_id: "") { (isSuccess, data) in
                self.hideLoadingView()
                if isSuccess{
                    self.gotoVC(VCs.HOME_NAV)
                }else{
                    if let message = data{
                        self.showToast(message)
                    }
                    print("neterror")
                }
            }
        }
    }
    
    @IBAction func googleSignupBtnClicked(_ sender: Any) {
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance()?.signIn()
    }
    
    @IBAction func faceBookSignupBtnClicked(_ sender: Any) {
        let loginManager = LoginManager()
        loginManager.logIn(permissions: ["public_profile", "email"], from: self) { [weak self](result, error) in
            
            if error != nil {
                print("Login failed")
            } else {
                if result!.isCancelled { print("login canceled") }
                else { self!.getFaceBookUserData() }
            }
        }
    }
    
    @IBAction func appleSignupBtnClicked(_ sender: Any) {
        if #available(iOS 13.0, *) {
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            let request = appleIDProvider.createRequest()
            request.requestedScopes = [.fullName, .email]
                
            let authorizationController = ASAuthorizationController(authorizationRequests: [request])
            authorizationController.delegate = self
            authorizationController.presentationContextProvider = self
            authorizationController.performRequests()
        } else {
            self.showToast("Updated system version")
        }
    }
    
    func getFaceBookUserData() {
        let connection = GraphRequestConnection()
        connection.add(GraphRequest(graphPath: "/me", parameters: ["fields": "id, name, email, first_name, last_name"])) { (httpResponse, result, error) in
            let jsonResponse = JSON(result)
            let socialId = jsonResponse["id"].intValue
            let id = jsonResponse["id"].stringValue
            let fname = jsonResponse["first_name"].stringValue
            let lname = jsonResponse["last_name"].stringValue
            let email = jsonResponse["email"].string ?? String(format: "%@@facebook.com", id)
            let name = jsonResponse["name"].string ?? "unknown"
            let photoUrlString = "http://graph.facebook.com/\(socialId)/picture?type=large"
            print("id: " + id)
            print("fname: " + fname)
            print("lname: " + lname)
            print("email: " + email)
            print("name: " + name)
            self.showLoadingView(vc: self)
            ApiManager.registerUser(first_name: fname, last_name: lname, email: email, phonenumber: "", password: name + Constants.FACEBOOK_PREFIX, photo: photoUrlString, account_type: AccountType.FACEBOOK, social_id: "\(socialId)") { (isSuccess, data) in
                if isSuccess{
                    self.gotoVC(VCs.HOME_NAV)
                }else{
                    if let message = data{
                        self.showToast(message)
                    }
                    print("neterror")
                }
            }
        }
        connection.start()
    }
}

//MARK:- GIDSignInDelegate
extension SignUpVC : GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if error == nil {
            guard let user = user else {return}
            let socialId = user.userID!                  // For client-side use only!
             // Safe to send to the server
            let fullName = user.profile.name!
            let firstname = fullName.split(separator: " ").first
            let lastname = fullName.split(separator: " ").last
            let email = user.profile.email!
            let photoUrlString = user.profile.imageURL(withDimension: 300)!.description
            self.showLoadingView(vc: self)
            ApiManager.registerUser(first_name: String(firstname ?? ""), last_name: String(lastname ?? ""), email: email, phonenumber: "", password: fullName + Constants.GOOGLE_PREFIX, photo: photoUrlString, account_type: AccountType.GOOGLE, social_id: socialId) { (isSuccess, data) in
                self.hideLoadingView()
                if isSuccess{
                    self.gotoVC(VCs.HOME_NAV)
                }else{
                    if let message = data{
                        self.showToast(message)
                    }
                    print("neterror")
                }
            }
        } else {
            print("GIDSignIn:--  ", error.localizedDescription)
        }
    }
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        viewController.modalPresentationStyle = .fullScreen
        present(viewController, animated: true) {}
    }
}

extension SignUpVC: ASAuthorizationControllerDelegate {
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        //Handle error here
    }
    
    // ASAuthorizationControllerDelegate function for successful authorization
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            // Create an account in your system.
            var userFirstName = ""
            var userLastName = ""
            var userEmail = ""
            let userIdentifier = appleIDCredential.user
            userFirstName = appleIDCredential.fullName?.givenName ?? ""
            userLastName = appleIDCredential.fullName?.familyName ?? ""
            userEmail = appleIDCredential.email ?? ""
            
            self.showLoadingView(vc: self)
            ApiManager.registerUser(first_name: userFirstName, last_name: userLastName, email: userEmail, phonenumber: "", password: "", photo: "", account_type: AccountType.APPLE, social_id: userIdentifier) { (isSuccess, data) in
                self.hideLoadingView()
                if isSuccess{
                    self.gotoVC(VCs.HOME_NAV)
                }else{
                    if let message = data{
                        self.showToast(message)
                    }
                    print("neterror")
                }
            }
        } else if let passwordCredential = authorization.credential as? ASPasswordCredential {
            // Sign in using an existing iCloud Keychain credential.
            let username = passwordCredential.user
            let password = passwordCredential.password
            print("\(username)\n \(password )")
            //Navigate to other view controller
        }
    }
}

extension SignUpVC : ASAuthorizationControllerPresentationContextProviding {
    /// - Tag: provide_presentation_anchor
    @available(iOS 13.0, *)
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
}





