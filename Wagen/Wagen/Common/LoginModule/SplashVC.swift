//
//  SplashVC.swift
//  EveraveUpdate
//
//  Created by Ubuntu on 12/10/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import UIKit
import SwiftyJSON
import Foundation
import _SwiftUIKitOverlayShims


class SplashVC: BaseVC {
    var networkStatus = 0
    //var ds_notifications = [NotiModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.asyncAfter(deadline: .now() + 20.0, execute: {
                self.networkStatus = 1
                if self.networkStatus == 1{
                    //self.showAlerMessage(message: "No  Internet connection")
                }
                else{
                    return
                }
            }
        )
        self.checkBackgrouond()
    }
   
    func checkBackgrouond(){
        self.showLoadingView(vc: self)
        if thisuser!.isValid{
            if UserDefault.getBool(key: PARAMS.IS_CHEKCED, defaultValue: false){
                if thisuser?.account_type == "3"{
                    self.showLoadingView(vc: self)
                    ApiManager.applelogin(email: thisuser?.email ?? "", first_name: thisuser?.first_name ?? "", last_name: thisuser?.last_name ?? "", social_id: thisuser?.social_id ?? "") { (isSuccess, data) in
                        self.hideLoadingView()
                        if isSuccess{
                            self.gotoVC(VCs.HOME_NAV)
                        }else{
                            thisuser!.clearUserInfo()
                            self.gotoVC(VCs.LOGIN_NAV)
                        }
                    }
                }else{
                    self.showLoadingView(vc: self)
                    ApiManager.login(email: thisuser!.email ?? "", password: thisuser!.password ?? "", account_type: thisuser!.account_type ?? "") { (isSuccess, data) in
                        self.hideLoadingView()
                        if isSuccess{
                            self.gotoVC(VCs.HOME_NAV)
                        }else{
                            thisuser!.clearUserInfo()
                            self.gotoVC(VCs.LOGIN_NAV)
                        }
                    }
                }
            }else{
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self.gotoVC(VCs.LOGIN_NAV)
                }
            }
        }else{
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.gotoVC(VCs.LOGIN_NAV)
            }
        }
    }
}
