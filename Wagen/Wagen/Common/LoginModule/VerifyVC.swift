//
//  VerifyVC.swift
//  Wagen
//
//  Created by top Dev on 10/24/20.
//

import UIKit
import KAPinField

class VerifyVC: BaseVC {

    @IBOutlet weak var edt_new_pwd: UITextField!
    @IBOutlet weak var edt_confirm_pwd: UITextField!
    @IBOutlet weak var cus_otp: KAPinField!
    var newpwd = ""
    var confirmpwd = ""
    var pincode: String?
    var email: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setEdtPlaceholder(edt_new_pwd, placeholderText: "", placeColor: .darkGray, padding: .left(10))
        setEdtPlaceholder(edt_confirm_pwd, placeholderText: "", placeColor: .darkGray, padding: .left(10))
    }
    
    @IBAction func confirmBtnClicked(_ sender: Any) {
        if let pincode = self.pincode{
            if pincode.isEmpty || pincode != cus_otp.text{
                self.showToast(Messages.ENTER_VERIFY_CODE)
            }
            
            else{
                self.showToast("Verify confirmed")
            }
        }
    }
    
    @IBAction func resetPwdBtnClickeedd(_ sender: Any) {
        newpwd = self.edt_new_pwd.text ?? ""
        confirmpwd = self.edt_confirm_pwd.text ?? ""
        if newpwd.isEmpty{
            self.showToast(Messages.PASSWORD_REQUIRE)
        }
        if confirmpwd.isEmpty || newpwd != confirmpwd{
            self.showToast(Messages.CONFIRM_PWD_CHECK)
        }
        else{
            if let email = self.email{
                self.showLoadingView(vc: self)
                ApiManager.updatepassword(email: email, password: newpwd) { (isSuccess) in
                    self.hideLoadingView()
                    if isSuccess{
                        self.gotoVC(VCs.LOGIN_NAV)
                    }else{
                        
                    }
                }
            }
        }
    }
}
