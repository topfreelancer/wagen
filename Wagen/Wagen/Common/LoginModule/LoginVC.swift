//
//  LoginVC.swift
//  EveraveUpdate
//
//  Created by Ubuntu on 12/10/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import Foundation
import UIKit
import IQKeyboardManagerSwift
import SwiftyJSON
import SwiftyUserDefaults
import GDCheckbox
import GoogleSignIn
import FacebookCore
import FBSDKLoginKit
import AuthenticationServices

class LoginVC: BaseVC, UITextFieldDelegate,UINavigationControllerDelegate {
    
    @IBOutlet weak var lbl_signin: UILabel!
    @IBOutlet weak var signBtnView: UIView!
    @IBOutlet weak var edt_email: UITextField!
    @IBOutlet weak var edtPwd: UITextField!
    @IBOutlet weak var btn_back: UIButton!
    @IBOutlet weak var imv_backBtn: UIImageView!
    @IBOutlet weak var cus_check: GDCheckbox!
    @IBOutlet weak var btn_apple: UIButton!
    var is_checked: Bool = false
    var email = ""
    var password = ""
    var account_type = AccountType.GENERAL
    //var ds_notifications = [NotiModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        editInit()
        loadLayout()
        
        if #available(iOS 13.0, *) {
//            let btnAuthorization = ASAuthorizationAppleIDButton()
//            btnAuthorization.frame = self.btn_apple.frame
//            btnAuthorization.center = self.view.center
//            //btnAuthorization.center = self.btn_apple.center
//            btnAuthorization.addTarget(self, action: #selector(handleLogInWithAppleIDButtonPress), for: .touchUpInside)
//            self.view.addSubview(btnAuthorization)
            //performExistingAccountSetupFlows()
        } else {
            // Fallback on earlier versions
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.hideNavBar()
    }
    
    func editInit() {
        setEdtPlaceholder(edt_email, placeholderText: "", placeColor: UIColor.lightGray, padding: .left(10))
        setEdtPlaceholder(edtPwd, placeholderText: "", placeColor: UIColor.lightGray, padding: .left(10))
    }
    
    func loadLayout() {
        self.hideKeyboardWhenTappedAround()
        //self.edt_email.text =  "test1@gmail.com"
        //self.edtPwd.text = "123"
    }
    
    @IBAction func checkBtnClicked(_ sender: Any) {
        self.is_checked = !self.is_checked
    }
    
    @IBAction func signInBtnClicked(_ sender: Any) {
        email = self.edt_email.text ?? ""
        password = self.edtPwd.text ?? ""
        if email.isEmpty{
            self.showToast(Messages.EMAIL_REQUIRE)
            return
        }
        if !email.isValidEmail(){
            self.showToast(Messages.VALID_EMAIL_REQUIRE)
            return
        }
        if password.isEmpty{
            self.showToast(Messages.PASSWORD_REQUIRE)
            return
        }
        else{
            self.showLoadingView(vc: self)
            ApiManager.login(email: self.email, password: self.password, account_type: self.account_type) { (isSuccess, message) in
                self.hideLoadingView()
                if isSuccess{
                    UserDefault.setBool(key: PARAMS.IS_CHEKCED, value: self.is_checked)
                    self.gotoVC(VCs.HOME_NAV)
                }else{
                    if let message = message{
                        self.showToast(message)
                    }
                }
            }
        }
    }
   
    @IBAction func gotoSignUp(_ sender: Any) {
        gotoNavPresent(VCs.SINGUP,fullscreen: true, animated: true)
    }
    
    @IBAction func gotoForgot(_ sender: Any) {
        self.gotoNavPresent(VCs.FORGOT, fullscreen: true, animated: true)
    }
    
    @IBAction func googleSigninBtnClicked(_ sender: Any) {
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance()?.signIn()
    }
    
    @IBAction func faceBookSigninBtnClicked(_ sender: Any) {
        let loginManager = LoginManager()
        loginManager.logIn(permissions: ["public_profile", "email"], from: self) { [weak self](result, error) in
            
            if error != nil {
                print("Login failed")
            } else {
                if result!.isCancelled { print("login canceled") }
                else { self!.getFaceBookUserData() }
            }
        }
    }
    
    @objc private func handleLogInWithAppleIDButtonPress() {
        if #available(iOS 13.0, *) {
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            let request = appleIDProvider.createRequest()
            request.requestedScopes = [.fullName, .email]
                
            let authorizationController = ASAuthorizationController(authorizationRequests: [request])
            authorizationController.delegate = self
            authorizationController.presentationContextProvider = self
            authorizationController.performRequests()
        } else {
            // Fallback on earlier versions
        }
    }
    
    @IBAction func appleSigninBtnClicked(_ sender: Any) {
        if #available(iOS 13.0, *) {
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            let request = appleIDProvider.createRequest()
            request.requestedScopes = [.fullName, .email]
                
            let authorizationController = ASAuthorizationController(authorizationRequests: [request])
            authorizationController.delegate = self
            authorizationController.presentationContextProvider = self
            authorizationController.performRequests()
        } else {
            self.showToast("Updated system version")
        }
    }
    
    private func performExistingAccountSetupFlows() {
        // Prepare requests for both Apple ID and password providers.
        if #available(iOS 13.0, *) {
            let requests = [ASAuthorizationAppleIDProvider().createRequest(), ASAuthorizationPasswordProvider().createRequest()]
            // Create an authorization controller with the given requests.
            let authorizationController = ASAuthorizationController(authorizationRequests: requests)
            authorizationController.delegate = self
            authorizationController.presentationContextProvider = self
            authorizationController.performRequests()
        } else {
            // Fallback on earlier versions
        }
    }
    
    func getFaceBookUserData() {
        let connection = GraphRequestConnection()
        connection.add(GraphRequest(graphPath: "/me", parameters: ["fields": "id, name, email, first_name, last_name"])) { (httpResponse, result, error) in
            let jsonResponse = JSON(result as Any)
            let socialId = jsonResponse["id"].intValue
            let id = jsonResponse["id"].stringValue
            let fname = jsonResponse["first_name"].stringValue
            let lname = jsonResponse["last_name"].stringValue
            let email = jsonResponse["email"].string ?? String(format: "%@@facebook.com", id)
            let name = jsonResponse["name"].string ?? "unknown"
            let photoUrlString = "http://graph.facebook.com/\(socialId)/picture?type=large"
            print("id: " + id)
            print("fname: " + fname)
            print("lname: " + lname)
            print("email: " + email)
            print("name: " + name)
            self.showLoadingView(vc: self)
            
            ApiManager.socialLogin(firstname: fname, lastname: lname, email: email, account_type: AccountType.FACEBOOK, photo: photoUrlString) { (isSuccess, message) in
                self.hideLoadingView()
                if isSuccess{
                    UserDefault.setBool(key: PARAMS.IS_CHEKCED, value: self.is_checked)
                    self.gotoVC(VCs.HOME_NAV)
                }else{
                    if let message = message{
                        self.showToast(message)
                    }
                }
            }
        }
        connection.start()
    }
}

//MARK:- GIDSignInDelegate
extension LoginVC : GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if error == nil {
            guard let user = user else {return}
            //let socialId = user.userID!                  // For client-side use only!
             // Safe to send to the server
            let fullName = user.profile.name!
            var firstname = ""
            var lastname = ""
            firstname = String(fullName.split(separator: " ").first ?? "")
            lastname = String(fullName.split(separator: " ").last ?? "")
            let email = user.profile.email!
            let photoUrlString = user.profile.imageURL(withDimension: 300)!.description
            self.showLoadingView(vc: self)
            ApiManager.socialLogin(firstname: firstname, lastname: lastname, email: email, account_type: AccountType.GOOGLE, photo: photoUrlString) { (isSuccess, message) in
                self.hideLoadingView()
                if isSuccess{
                    UserDefault.setBool(key: PARAMS.IS_CHEKCED, value: self.is_checked)
                    self.gotoVC(VCs.HOME_NAV)
                }else{
                    if let message = message{
                        self.showToast(message)
                    }
                }
            }
        } else {
            print("GIDSignIn:--  ", error.localizedDescription)
        }
    }
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        viewController.modalPresentationStyle = .fullScreen
        present(viewController, animated: true) {}
    }
}

extension LoginVC: ASAuthorizationControllerDelegate {
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        //Handle error here
    }
    
    // ASAuthorizationControllerDelegate function for successful authorization
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            
            var userFirstName = ""
            var userLastName = ""
            var userEmail = ""
            
            let userIdentifier = appleIDCredential.user
            userFirstName = appleIDCredential.fullName?.givenName ?? ""
            userLastName = appleIDCredential.fullName?.familyName ?? ""
            userEmail = appleIDCredential.email ?? ""
            print("\(userIdentifier)\n \(userFirstName)\n \(userLastName)\n \(userEmail)")
            self.showLoadingView(vc: self)
            ApiManager.applelogin(email: userEmail, first_name: userFirstName, last_name: userLastName, social_id: userIdentifier) { (isSuccess, data) in
                self.hideLoadingView()
                if isSuccess{
                    UserDefault.setBool(key: PARAMS.IS_CHEKCED, value: self.is_checked)
                    self.gotoVC(VCs.HOME_NAV)
                }else{
                    if let message = data{
                        self.showToast(message)
                    }
                }
            }
        } else if let passwordCredential = authorization.credential as? ASPasswordCredential {
            // Sign in using an existing iCloud Keychain credential.
            let username = passwordCredential.user
            let password = passwordCredential.password
            print("\(username)\n \(password )")
            //Navigate to other view controller
        }
    }
}

extension LoginVC : ASAuthorizationControllerPresentationContextProviding {
    /// - Tag: provide_presentation_anchor
    @available(iOS 13.0, *)
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
}
