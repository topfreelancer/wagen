//
//  Constants.swift
//  emoglass
//
//  Created by Mac on 7/7/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
import UIKit

var cars = [CarsModel]()
var packages = [PackagesModel]()
var promotions = [PromotionModel]()
var services = [ServicesModel]()
var cities = [CitiesModel]()
var memberships = [MembershipModel]()
var workshop = [WorkshopModel]()
var couponcodes = [CouponCodes]()
var weburls = WeburlModel()
var main_service_summary = SummaryModel()
var additional_service_summary = SummaryModel()


public let GOOGLE_PLACES_API_KEY = "AIzaSyDIy5OhjI6sI_SpqwMc-VIrdkWSRQfI6Ic"
struct Constants {
    static let PROGRESS_COLOR      = UIColor.black
    static let SAVE_ROOT_PATH      = "save_root"
    static let SCREEN_HEIGHT       = UIScreen.main.bounds.height
    static let SCREEN_WIDTH        = UIScreen.main.bounds.width
    static let deviceToken         = UIDevice.current.identifierForVendor!.uuidString
    // MARK: current timezone
    static var localTimeZoneIdentifier: String { return TimeZone.current.identifier }
    
    static let CLIENT_ID    = "910163104621-2g37v306mdeouc1311fkd72mmik8h0h5.apps.googleusercontent.com"
    static let GOOGLE_PREFIX    = "_google"
    static let FACEBOOK_PREFIX    = "_facebook"
    //static let MercadoPublicKey    = "TEST-9316000c-da0c-417d-93d6-c771452316d1"
    static let MercadoPublicKey    = "APP_USR-4d666710-cc54-44ab-bda3-5504a0eb4f2c"
}

struct AccountType {
    static let GENERAL  = "0"
    static let GOOGLE   = "1"
    static let FACEBOOK = "2"
    static let APPLE = "3"
}

struct VCs {
    static let ADDITIONAL_SERVICE = "AdditionalServiceVC"
    static let BOOKING_SUMMARY    = "BookingSummaryVC"
    static let CALENDAR           = "CalendarVC"
    static let EDIT_PROFILE       = "EditProfileVC"
    static let FORGOT             = "ForgotVC"
    static let GIVE_REVIEW        = "GiveReveiwVC"
    static let HOME               = "HomeVC"
    static let HOME_NAV           = "HomeNav"
    static let LOGIN              = "LoginVC"
    static let LOGIN_NAV          = "LoginNav"
    static let MY_BOOKING         = "MyBookingVC"
    static let SELECT_VEHICLE     = "SelectVehicleVC"
    static let SETTINGS           = "SettingsVC"
    static let SINGUP             = "SignUpVC"
    static let SPLASH             = "SplashVC"
    static let VERIFY             = "VerifyVC"
    static let WASH_PACKAGES      = "WashPackagesVC"
    static let WRITE_REVIEW       = "WriteReviewVC"
    static let MEMBERSHIP         = "MembershipVC"
}

struct TestData {
    static let images = ["https://i.pinimg.com/236x/0c/3d/bc/0c3dbc3fc00744e032fa8278e5e16741.jpg","https://i.pinimg.com/236x/4d/b0/10/4db0106cf43cea00b8c1c4c038d9f9b1.jpg","https://i.pinimg.com/236x/39/55/e9/3955e9f28508f563f1ae7e660f96b0a8.jpg","https://i.pinimg.com/236x/14/55/81/14558108de6f31aa19f5984bfbd3cbfc.jpg","https://i.pinimg.com/236x/3d/96/f1/3d96f14098bef76fdd23f22f353099cb.jpg","https://i.pinimg.com/236x/16/d1/91/16d191a2120592b0920f243e543ab5ed.jpg","https://i.pinimg.com/236x/48/ec/56/48ec56dd4aaa2508ef71ad3d8ff8a2ff.jpg","https://i.pinimg.com/236x/78/4b/30/784b30ff549ca6598e86ca55ffc5bd9f.jpg","https://i.pinimg.com/236x/61/be/9d/61be9d15e87fd945b7339ab598d655aa.jpg","https://i.pinimg.com/236x/55/64/b5/5564b54e232a080c4fd6c025c361d379.jpg"]
    static let userNames = ["Paul Newman","Hems Bett","Robert De Niro","Tom Ellis","Brad Pitt","Bloomberg kevin","Gerard Butler","Peter Kavinsky","Richard Madden","Johnny Depp"]
    /*static let times = ["00:00 AM","01:00 AM","02:00 AM","03:00 AM","04:00 AM","05:00 AM","06:00 AM","07:00 AM","08:00 AM","09:00 AM","10:00 AM","11:00 AM","12:00 AM","01:00 PM","02:00 PM","03:00 PM","04:00 PM","05:00 PM","06:00 PM","07:00 PM","08:00 PM","09:00 PM","10:00 PM","11:00 PM"]*/
    static let times = ["09:30","10:30","11:30","12:30","13:30","14:30","15:30","16:30","17:30"]
    static let duration = ["50Mins","60Mins","75Mins","90Mins","50Mins","60Mins","75Mins","90Mins","50Mins","60Mins"]
    static let userLocation = ["Fukuoka","Iyo","Gunma","Sumitomo Mitsui","Wakayama","Hokkaido","Kanagawa","Okayama","Tokyo","Suruga"]
    static let content = ["content1","content2","content3","content4","content5","content6","content7","content8","content9","content10"]
    static let price = ["100.000", "130.000", "450.025", "670.055", "430.000", "230.000", "340.030", "340.000", "640.000", "670.090"]
    static let date = ["23/10/2020","24/10/2020","25/10/2020","25/10/2020","24/10/2020","24/10/2020","23/10/2020","24/10/2020","23/10/2020","25/10/2020"]
    static let service_type = ["Lavado Exterior Express","Lavado Full ","Lavado Premium","Lavado Deluxe"]
    
    static let service_content = ["Lavado Exterior 2 en 1 \nLavado de vidrios y llantas","Lavado Exterior 2 en 1 \nLavado de vidrios y llantas \nAspirado interior \nLimpieza de tablero y panel de \ninstrumentos","Lavado Exterior 2 en 1 \nLavado de vidrios y llantas \nAspirado interior \nLimpieza de tablero y panel de \ninstrumentos \nLimpieza de tapíz o renovador de \ncuero \nProtector de foco","Lavado con Shampoo Special \nDetailing \nLavado de vidrios con aplicación de \nRain-X \nLimpieza de llantas \nRenovador de gomas interiores \nAspirado interior \nRenovador de neumáticos \nLimpieza de tablero y panel de \ninstrumentos \nLimpieza de tapíz o renovador de \ncuero \nProtector de foco \nAplicación de polímero hidrofóbico"]
    static let city = ["Las Condes","Vitacura","Chicureo"]
    static let address = ["José Alcalde Délano 10.581, Lo Barnechea 23"]
    
    static let additional_service_type = ["Descontaminado con ClayBar y quickdetailing","Limpieza de motor a mano","Sellado de motor","Pulido localizado (por pieza)", "Pulido reinex de todos los vidrios", "Aplicación de Plastidip a llantas (pintura de llantas) c/u"]
}

struct Messages {
    static let ADD_PHONE_NUMBER_EMAIL          = "Antes de reservar el servicio, debe registrar su número de teléfono móvil y correo electrónico en su perfil."
    static let CHOOSE_GALLERY            = "Elegir desde la Galería"
    static let CONFIRM_PASSWORD_MATCH    = "Por favor confirma tu contraseña"
    static let CONFIRM_PWD_CHECK         = "Por favor confirma tu contraseña"
    static let EMAIL_REQUIRE             = "Por favor ingresa tu correo"
    static let ENTER_VERIFY_CODE         = "Por favor ingresa el código de verificación correcto"
    static let FIRST_NAME_REQUIRE        = "Por favor ingresa tu nombre"
    static let INPUT_CORRECT_COUPON_CODE = "Por favor ingresa el cupón de descuento"
    static let LAST_NAME_REQUIRE         = "Por favor ingresa tu apellido"
    static let NETISSUE                  = "Algo salió mal."
    static let PASSWORD_REQUIRE          = "Por favor ingresa tu contraseña"
    static let PHONE_NUMBER_REQUIRE      = "Por favor ingresa tu celular"
    static let PURCHASE_MEMBERSHIP       = "Plan mensual"
    static let SELECT_VEHICLE            = "Por favor selecciona un auto"
    static let TAKE_PHOTO                = "Toma un Foto"
    static let VALID_EMAIL_REQUIRE       = "Por favor ingresa tu correo"
}

enum PayType {
    case membership_pay
    case direct_pay
    case null
}

var pay_type:PayType = .null

var paidmembership_model:MembershipModel?
var appBundleID = Bundle.main.bundleIdentifier!




