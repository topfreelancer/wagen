//
//  BaseVC.swift
//  TagTeam
//
//  Created by Admin on 9/30/20.
//

import UIKit
import Toast_Swift
import MBProgressHUD

class BaseVC: UIViewController {
    
    var hud: MBProgressHUD?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func showNavBar() {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    func hideNavBar() {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
//    func presentNextVC(storyboardName : String, vcName : String) {
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
//            let storyBoard : UIStoryboard = UIStoryboard(name: storyboardName, bundle: nil)
//            let vc = storyBoard.instantiateViewController(identifier: vcName)
//            vc.modalPresentationStyle = .fullScreen
//            self.present(vc, animated: true, completion: nil)
//        }
//    }
//    
//    func pushNextVC(storyboardName : String, vcName : String) {
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
//            let storyBoard : UIStoryboard = UIStoryboard(name: storyboardName, bundle: nil)
//            let vc = storyBoard.instantiateViewController(identifier: vcName)
//            vc.modalPresentationStyle = .fullScreen
//            self.navigationController?.pushViewController(vc, animated: true)
//        }
//    }
//    
//    func pushNextVC_NoAnimation(storyboardName : String, vcName : String) {
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
//            let storyBoard : UIStoryboard = UIStoryboard(name: storyboardName, bundle: nil)
//            let vc = storyBoard.instantiateViewController(identifier: vcName)
//            vc.modalPresentationStyle = .fullScreen
//            self.navigationController?.pushViewController(vc, animated: false)
//        }
//    }
//    
//    func presentNextVC_NotFull(storyboardName : String, vcName : String) {
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
//            let storyBoard : UIStoryboard = UIStoryboard(name: storyboardName, bundle: nil)
//            let vc = storyBoard.instantiateViewController(identifier: vcName)
//            self.present(vc, animated: true, completion: nil)
//        }
//    }
    
    func gotoNavPresent(_ vcname : String, fullscreen: Bool, animated: Bool) {
        let toVC = self.storyboard?.instantiateViewController(withIdentifier: vcname)
        if fullscreen{
            toVC?.modalPresentationStyle = .fullScreen
        }else{
            toVC?.modalPresentationStyle = .popover
        }
        self.navigationController?.pushViewController(toVC!, animated: animated)
    }
    
    func createVC(_ vcname : String) -> UIViewController?{
        let toVC = self.storyboard?.instantiateViewController(withIdentifier: vcname)
        return toVC
    }
    
    func gotoNavPresentWithVC(_ vc : UIViewController?, fullscreen: Bool) {
        if let vc = vc{
            if fullscreen{
                vc.modalPresentationStyle = .fullScreen
            }else{
                vc.modalPresentationStyle = .popover
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func setTitle( _ str: String?) {
        if let str = str{
            self.title = str
        }
    }
    
    //MARK:- Toast function
    func showToast(_ message : String) {
        self.view.makeToast(message)
    }
    
    func showToast(_ message : String, duration: TimeInterval = ToastManager.shared.duration, position: ToastPosition = .bottom) {
        self.view.makeToast(message, duration: duration, position: position)
    }
    
    func showToastCenter(_ message : String, duration: TimeInterval = ToastManager.shared.duration) {
        showToast(message, duration: duration, position: .center)
    }
    
    func setEdtPlaceholder(_ edittextfield : UITextField , placeholderText : String, placeColor : UIColor, padding: UITextField.PaddingSide)  {
        edittextfield.attributedPlaceholder = NSAttributedString(string: placeholderText,
        attributes: [NSAttributedString.Key.foregroundColor: placeColor])
        edittextfield.addPadding(padding)
    }
    
    func gotoVC(_ nameVC: String){
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let toVC = storyBoard.instantiateViewController( withIdentifier: nameVC)
        toVC.modalPresentationStyle = .fullScreen
        self.present(toVC, animated: true, completion: nil)
    }
    
    func showProgressHUDHUD(view : UIView, mode: MBProgressHUDMode = .annularDeterminate) -> MBProgressHUD {
    
        let hud = MBProgressHUD .showAdded(to:view, animated: true)
        hud.mode = mode
        hud.label.text = "Loading";
        hud.animationType = .zoomIn
        hud.tintColor = UIColor.red
        hud.contentColor = .darkGray
        return hud
    }
    
    func hideLoadingView() {
       if let hud = hud {
           hud.hide(animated: true)
       }
    }
    
    func removeSpecialCharsFromString(text: String) -> String {
        let okayChars : Set<Character> =
            Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890+-*=(),.:!_")
        return String(text.filter {okayChars.contains($0) })
    }
    
    func showLoadingView(vc: UIViewController, label: String = "") {
        
        hud = MBProgressHUD .showAdded(to: vc.view, animated: true)
        
        if label != "" {
            hud!.label.text = label
        }
        hud!.mode = .indeterminate
        hud!.animationType = .zoomIn
        hud!.bezelView.color = .clear
        hud!.bezelView.style = .solidColor
    }
    
    func gotoWebViewWithProgressBar(_ link: String)  {
        let browser = KAWebBrowser()
        show(browser, sender: nil)
        browser.loadURLString(link)
    }
    
    func showCustomDialog(titlee: String, content: String) {
        let ratingVC = RatingViewController(nibName: "RatingViewController", bundle: nil)
        ratingVC.titlee = titlee
        ratingVC.content = content
        ratingVC.modalPresentationStyle = .overFullScreen
        self.present(ratingVC, animated: false, completion: nil)
    }
}

class string_spaces {
    class func spacesWithCount(_ num: Int) -> String {
        let spaces = String(repeating: " ", count: num)
        return spaces
    }
}



