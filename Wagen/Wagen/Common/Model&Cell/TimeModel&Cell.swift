//
//  TimeModel&Cell.swift
//  Wagen
//
//  Created by top Dev on 10/25/20.
//

import Foundation
import UIKit

class timeModel: NSObject {
        var str_time: String?
       override init() {
           super.init()
            str_time = ""
       }
       
    init(_ str_time: String?) {
        self.str_time = str_time
    }
}


class timeCell: UICollectionViewCell {
    @IBOutlet weak var backView: UIView!
    @IBOutlet var lbl_time: UILabel!
    
    var entity: timeModel!{
        didSet{
            self.lbl_time.text = entity.str_time
        }
    }
    override var isSelected: Bool {
        didSet {
            self.backView.backgroundColor = isSelected ? UIColor.init(named: "color_primary") : UIColor.init(named: "color_edt_back")
            self.lbl_time.textColor = isSelected ? .white : UIColor.init(named: "color_primary")
        }
    }
}
