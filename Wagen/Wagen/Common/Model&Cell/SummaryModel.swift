//
//  SummaryModel.swift
//  Wagen
//
//  Created by top Dev on 29.11.2020.
//

import Foundation
import SwiftyJSON

class SummaryModel: NSObject {
    
    var id : String?
    var address : String?
    var datee : String?
    var timee : String?
    var duration : String?
    var price : String?
    var car_id: String?
    var package_id: String?
    var service_ids: String?
    var service_qty: String?
    var order_type: String?
    var order_city: String?
    var coupon_id: String?
    var discount_percent: String?
    
    override init() {
       super.init()
        id = ""
        address = ""
        datee = ""
        duration = "0"
        price = "0"
        car_id = "1"
        package_id = ""
        service_ids = ""
        order_type = OrderType.workshop
        order_city = ""
        coupon_id = "-1"
        discount_percent = "0"
        service_qty = ""
    }
    
    init(id: String,workshop_address: String, timee: String,  address: String, datee: String, duration: String, price: String, car_id: String, package_id: String, service_ids: String, order_type: String, order_city: String,  coupon_id: String, discount_percent: String,serviceqty: String) {
        self.id = id
        self.timee = timee
        self.address = address
        self.datee = datee
        self.duration = duration
        self.price = price
        self.car_id = car_id
        self.package_id = package_id
        self.service_ids = service_ids
        self.order_type = order_type
        self.order_city = order_city
        self.coupon_id = coupon_id
        self.discount_percent = discount_percent
        self.service_qty = serviceqty
    }
}
