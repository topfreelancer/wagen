//
//  WashPackageModel&Cell.swift
//  Wagen
//
//  Created by top Dev on 10/25/20.
//

import Foundation
import SwiftyJSON

class PackagesModel: NSObject {
    
    var package_id : String?
    var delete_status : String?
    var package_available_for_home : String?
    var package_description : String?
    var package_name : String?
    var package_prices = [PackagePriceModel]()
    var package_time : String?

    override init() {
       super.init()
        package_id = ""
        delete_status = ""
        package_available_for_home = ""
        package_description = ""
        package_name = ""
        package_prices = [PackagePriceModel()]
        package_time = ""
    }
    
    init(_ json : JSON) {
        self.package_id = json["package_id"].stringValue
        self.delete_status = json["delete_status"].stringValue
        self.package_available_for_home = json["package_available_for_home"].stringValue
        self.package_description = json["package_description"].stringValue
        self.package_name = json["package_name"].stringValue
        if let prices = json["package_prices"].arrayObject{
            self.package_prices.removeAll()
            for one in prices{
                self.package_prices.append(PackagePriceModel(JSON(one)))
            }
        }
        self.package_time = json["package_time"].stringValue
    }
}

class PackagePriceModel: NSObject {
    
    var car_id : String?
    var delete_status : String?
    var package_id : String?
    var package_price_id : String?
    var price : String?
    var price_home : String?

    override init() {
       super.init()
        car_id = ""
        delete_status = ""
        package_id = ""
        package_price_id = ""
        price = ""
        price_home = ""
    }
    
    init(_ json : JSON) {
        self.car_id = json["car_id"].stringValue
        self.delete_status = json["delete_status"].stringValue
        self.package_id = json["package_id"].stringValue
        self.package_price_id = json["package_price_id"].stringValue
        self.price = json["price"].stringValue
        self.price_home = json["price_home"].stringValue
    }
}




class WashPackageModel{
    
    var id : String?
    var service_type: String?
    var price: String?
    var price_home: String?
    var duration: String?
    var service_content: String?
    var selected: Bool
    var package_available_for_home : Bool = false
    
    init(id: String,service_type: String, price: String,price_home: String, duration: String, service_content: String, selected: Bool, package_available_for_home: Bool) {
        self.id = id
        self.service_type = service_type
        self.price = price
        self.duration = duration
        self.service_content = service_content
        self.selected = selected
        self.price_home = price_home
        self.package_available_for_home = package_available_for_home
    }
}


class WashPackageCell: UITableViewCell {
    
    @IBOutlet weak var lbl_service_type: UILabel!
    @IBOutlet weak var lbl_price_duration: UILabel!
    @IBOutlet weak var lbl_service_content: UILabel!
    @IBOutlet weak var btn_book: UIButton!
    @IBOutlet weak var lbl_btn_caption: UILabel!
    
    func setEntiy(_ index: Int, entity: WashPackageModel) {// 0: price, 1: price_home
        lbl_service_type.text = entity.service_type
        if index == 0{
            lbl_price_duration.text = "$" + splitWithDot(entity.price!) + string_spaces.spacesWithCount(3) + entity.duration! + "Mins"
        }else{
            lbl_price_duration.text = "$" + splitWithDot(entity.price_home!) + string_spaces.spacesWithCount(3) + entity.duration! + "Mins"
        }
        lbl_btn_caption.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 2)
        if let content = entity.service_content{
            lbl_service_content.text = content.replacingOccurrences(of: "_", with: "\n", options: .literal, range: nil)
        }
        if entity.selected{
            btn_book.backgroundColor = UIColor.init(named: "color_primary")
            lbl_btn_caption.textColor = .white
        }else{
            btn_book.backgroundColor = UIColor.init(named: "color_edt_back")
            lbl_btn_caption.textColor = .black
        }
    }
}





