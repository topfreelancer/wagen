//
//  PromotionModel.swift
//  Wagen
//
//  Created by top Dev on 25.11.2020.
//

import Foundation
import SwiftyJSON

class PromotionModel: NSObject {
    
    var id : String?
    var banner_image : String?
    var created_datetime : String?
    var delete_status : String?
    var descriptionn : String?
    var full_image : String?
    var title : String?

    override init() {
       super.init()
        id = ""
        banner_image = ""
        created_datetime = ""
        delete_status = ""
        descriptionn = ""
        full_image = ""
        title = ""
    }
    
    init(_ json : JSON) {
        self.id = json["id"].stringValue
        self.banner_image = json["banner_image"].stringValue
        self.created_datetime = json["created_datetime"].stringValue
        self.delete_status = json["delete_status"].stringValue
        self.descriptionn = json["description"].stringValue
        self.full_image = json["full_image"].stringValue
        self.title = json["title"].stringValue
    }
}
