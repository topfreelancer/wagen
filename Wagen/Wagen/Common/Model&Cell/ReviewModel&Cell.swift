//
//  NotiModel&Cell.swift
//  emoglass
//
//  Created by Mac on 7/8/20.
//  Copyright © 2020 Mac. All rights reserved.
//


import Foundation
import Kingfisher

class ReviewModel: NSObject{
    
    var id : String?
    var date: String?
    var time: String?
    var duration: String?
    var price: String?
    var car_id: String?
    var car_name: String?
    var car_photo: String?
    var package_id: String?
    var order_type: String?
    var services: [ServicesModel]?
    
    override init() {
       super.init()
        id = ""
        date = ""
        time = ""
        duration = ""
        price = ""
        car_id = ""
        car_name = ""
        car_photo = ""
        package_id = ""
        order_type = ""
        services = nil
    }
    
    init(id: String,date: String, time: String, duration: String, price: String,car_id: String,  car_name: String, car_photo: String, package_id: String,order_type: String, services: [ServicesModel]) {
        self.id = id
        self.time = time
        self.date = date
        self.duration = duration
        self.price = price
        self.car_id = car_id
        self.car_name = car_name
        self.car_photo = car_photo
        self.package_id = package_id
        self.order_type = order_type
        self.services = services
    }
}


class ReviewCell: UITableViewCell {
    
    @IBOutlet weak var lbl_service_name: UILabel!
    @IBOutlet weak var lbl_duration: UILabel!
    @IBOutlet weak var lbl_price: UILabel!
    
    func setReview( _ servicename: String, price: String, duration: String)  {
        lbl_service_name.text = servicename
        lbl_duration.text = duration + "Mins"
        lbl_price.text = splitWithDot(price) + "CLP"
    }
}




