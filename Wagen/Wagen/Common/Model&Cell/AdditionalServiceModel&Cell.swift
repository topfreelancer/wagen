//
//  AdditionalServiceModel&Cell.swift
//  Wagen
//
//  Created by top Dev on 10/25/20.
//

import Foundation
import Kingfisher
import GMStepper

class AdditionalServiceModel{
    
    var id : String?
    var additional_service_type: String?
    var price: String?
    var duration: String?
    var cus_status: String?
    var selected: Bool
    var countt: Int
    
    init(id: String,additional_service_type: String, price: String, duration: String, selected: Bool, cus_status: String, countt: Int) {
        self.id = id
        self.additional_service_type = additional_service_type
        self.price = price
        self.duration = duration
        self.selected = selected
        self.cus_status = cus_status
        self.countt = countt
    }
}


class AdditionalServiceCell: UITableViewCell {
    
    @IBOutlet weak var lbl_additional_service_type: UILabel!
    @IBOutlet weak var lbl_price_duration: UILabel!
    //@IBOutlet weak var btn_select: UIButton!
    @IBOutlet weak var btn_select: dropShadowDarkButton!
    @IBOutlet weak var lbl_btn_caption: UILabel!
    @IBOutlet weak var cus_steppter: GMStepper!
    @IBOutlet weak var uiv_outline: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    var entity:AdditionalServiceModel!{
        didSet{
            lbl_additional_service_type.text = entity.additional_service_type
            lbl_price_duration.text = "$" + splitWithDot(entity.price!) + string_spaces.spacesWithCount(3) + entity.duration! + "Mins"
            
            if entity.selected{
                btn_select.backgroundColor = UIColor.init(named: "color_edt_back")
                btn_select.borderColor = .darkGray
                btn_select.borderWidth = 0.5
                lbl_btn_caption.textColor = .black
                lbl_btn_caption.text = "seleccionado"
            }else{
                btn_select.backgroundColor = UIColor.init(named: "color_primary")
                btn_select.borderWidth = 0.5
                btn_select.borderColor = .clear
                lbl_btn_caption.textColor = .white
                lbl_btn_caption.text = "seleccionar"
            }
        }
    }
}

