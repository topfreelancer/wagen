//
//  OrderModel.swift
//  Wagen
//
//  Created by top Dev on 01.12.2020.
//


import Foundation
import SwiftyJSON

class OrderModel: NSObject {
    
    var order_date : String?
    var order_time : String?
    var duration_time : String?
    var total_price : String?
    var car_id : String?
    var package_id : String?
    var service_ids: String?
    var order_type: String?
    var order_city: String?
    var order_address: String?
    var payment_method: String?
    var service_qty: String?
    var coupon_id: String?
    var discount_percent: String?
    var paid_price: String?
    
    override init() {
       super.init()
        car_id           = "0"
        coupon_id        = "-1"
        discount_percent = "0"
        duration_time    = ""
        order_address    = ""
        order_city       = ""
        order_date       = ""
        order_time       = ""
        order_type       = ""
        package_id       = "1"
        paid_price       = ""
        payment_method   = PaymentType.directpay
        service_ids      = ""
        service_qty      = ""
        total_price      = ""
    }
    
    init(order_date: String,order_time: String, duration_time: String,  total_price: String, car_id: String, package_id: String, service_ids: String, order_type: String, order_city: String, order_address: String, payment_method: String, service_qty: String,  coupon_id: String, discount_percent: String,paid_price: String) {
        
        self.car_id           = car_id
        self.coupon_id        = coupon_id
        self.discount_percent = discount_percent
        self.duration_time    = duration_time
        self.order_address    = order_address
        self.order_city       = order_city
        self.order_date       = order_date
        self.order_time       = order_time
        self.order_type       = order_type
        self.package_id       = package_id
        self.paid_price       = paid_price
        self.payment_method   = payment_method
        self.service_ids      = service_ids
        self.service_qty      = service_qty
        self.total_price      = total_price
    }
}
