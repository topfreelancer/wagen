//
//  WorkshopModel.swift
//  Wagen
//
//  Created by top Dev on 25.11.2020.
//

import Foundation
import SwiftyJSON

class WorkshopModel: NSObject {
    
    var id : String?
    var name : String?
    var delete_status : String?
    var order_limit : String?

    override init() {
       super.init()
        id = ""
        name = ""
        delete_status = ""
        order_limit = ""
    }
    
    init(_ json : JSON) {
        self.id = json["id"].stringValue
        self.name = json["name"].stringValue
        self.delete_status = json["delete_status"].stringValue
        self.order_limit = json["order_limit"].stringValue
    }
}
