//
//  WeburlModel.swift
//  Wagen
//
//  Created by top Dev on 22.02.2021.
//

import Foundation
import SwiftyJSON

class WeburlModel: NSObject {
    
    var id : String?
    var introl_url : String?
    var terms_url : String?
    var privacy_url : String?
    var aboutservice_url : String?
    
    
    override init() {
       super.init()
        id               = nil
        introl_url       = nil
        terms_url        = nil
        privacy_url      = nil
        aboutservice_url = nil
    }
    
    init(_ json: JSON) {
        self.id               = json["id"].stringValue
        self.introl_url       = json["intro_url"].stringValue
        self.terms_url        = json["terms_url"].stringValue
        self.privacy_url      = json["privacy_url"].stringValue
        self.aboutservice_url = json["aboutservicios_url"].stringValue
    }
}
