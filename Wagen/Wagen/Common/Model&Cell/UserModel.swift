//
//  UserModel.swift
//  EveraveUpdate
//
//  Created by Ubuntu on 1/18/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import Foundation
import SwiftyJSON

class UserModel: NSObject {                   
    
    var user_id : String?
    var social_id : String?
    var photo : String?
    var phone_verifystatus : String?
    var phonenumber : String?
    var password : String?
    var membership_last_renew : String?
    var membership_id : String?
    var membership_count : String?
    var last_name : String?
    var first_name : String?
    var facebook_deletecode : String?
    var email : String?
    var delete_status : String?
    var account_type : String?

    override init() {
       super.init()
         user_id = "0"
         social_id = "0"
         photo = ""
         phone_verifystatus = ""
         phonenumber = ""
         password = ""
         membership_last_renew = ""
         membership_id = ""
         membership_count = ""
         last_name = ""
         first_name = ""
         facebook_deletecode = ""
         email = ""
         delete_status = "0"
         account_type = "0"
    }
    
    init(_ json : JSON) {
        self.user_id = json["user_id"].stringValue
        self.social_id = json["social_id"].stringValue
        self.photo = json["photo"].stringValue
        self.phone_verifystatus = json["phone_verifystatus"].stringValue
        self.phonenumber = json["phonenumber"].stringValue
        self.password = json["password"].stringValue
        self.membership_last_renew = json["membership_last_renew"].stringValue
        self.membership_id = json["membership_id"].stringValue
        self.membership_count = json["membership_count"].stringValue
        self.last_name = json["last_name"].stringValue
        self.first_name = json["first_name"].stringValue
        self.facebook_deletecode = json["facebook_deletecode"].stringValue
        self.email = json["email"].stringValue
        self.delete_status = json["delete_status"].stringValue
        self.account_type = json["account_type"].stringValue
    }
    // Check and returns if user is valid user or not
   var isValid: Bool {
       return user_id != nil && user_id != "0"
   }
    
    // Recover user credential from UserDefault
    func loadUserInfo() {
        user_id = UserDefault.getString(key:"user_id", defaultValue: "0")
        social_id = UserDefault.getString(key:"social_id", defaultValue: "0")
        photo = UserDefault.getString(key:"photo", defaultValue: "")
        phone_verifystatus = UserDefault.getString(key:"phone_verifystatus", defaultValue: "")
        phonenumber = UserDefault.getString(key:"phonenumber", defaultValue: "")
        password = UserDefault.getString(key:"password", defaultValue: "")
        membership_last_renew = UserDefault.getString(key:"membership_last_renew", defaultValue: "")
        membership_id = UserDefault.getString(key:"membership_id", defaultValue: "")
        membership_count = UserDefault.getString(key:"membership_count", defaultValue: "")
        last_name = UserDefault.getString(key:"last_name", defaultValue: "")
        first_name = UserDefault.getString(key:"first_name", defaultValue: "")
        facebook_deletecode = UserDefault.getString(key:"facebook_deletecode", defaultValue: "")
        email = UserDefault.getString(key:"email", defaultValue: "")
        delete_status = UserDefault.getString(key:"delete_status", defaultValue: "0")
        account_type = UserDefault.getString(key:"account_type", defaultValue: "0")
    }
    // Save user credential to UserDefault
    func saveUserInfo() {
        UserDefault.setString(key: "user_id", value: user_id)
        UserDefault.setString(key: "social_id", value: social_id)
        UserDefault.setString(key: "photo", value: photo)
        UserDefault.setString(key: "phone_verifystatus", value: phone_verifystatus)
        UserDefault.setString(key: "phonenumber", value: phonenumber)
        UserDefault.setString(key: "password", value: password)
        UserDefault.setString(key: "membership_last_renew", value: membership_last_renew)
        UserDefault.setString(key: "membership_id", value: membership_id)
        UserDefault.setString(key: "membership_count", value: membership_count)
        UserDefault.setString(key: "last_name", value: last_name)
        UserDefault.setString(key: "first_name", value: first_name)
        UserDefault.setString(key: "facebook_deletecode", value: facebook_deletecode)
        UserDefault.setString(key: "email", value: email)
        UserDefault.setString(key: "delete_status", value: delete_status)
        UserDefault.setString(key: "account_type", value: account_type)
    }
    // Clear save user credential
    func clearUserInfo() {
        user_id = "0"
        social_id = "0"
        photo = ""
        phone_verifystatus = ""
        phonenumber = ""
        password = ""
        membership_last_renew = ""
        membership_id = ""
        membership_count = ""
        last_name = ""
        first_name = ""
        facebook_deletecode = ""
        email = ""
        delete_status = "0"
        account_type = "0"
        
        UserDefault.setString(key: "user_id", value: nil)
        UserDefault.setString(key: "social_id", value: nil)
        UserDefault.setString(key: "photo", value: nil)
        UserDefault.setString(key: "phone_verifystatus", value: nil)
        UserDefault.setString(key: "phonenumber", value: nil)
        UserDefault.setString(key: "password", value: nil)
        UserDefault.setString(key: "membership_last_renew", value: nil)
        UserDefault.setString(key: "membership_id", value: nil)
        UserDefault.setString(key: "membership_count", value: nil)
        UserDefault.setString(key: "last_name", value: nil)
        UserDefault.setString(key: "first_name", value: nil)
        UserDefault.setString(key: "first_name", value: nil)
        UserDefault.setString(key: "email", value: nil)
        UserDefault.setString(key: "delete_status", value: nil)
        UserDefault.setString(key: "account_type", value: nil)
    }
}

