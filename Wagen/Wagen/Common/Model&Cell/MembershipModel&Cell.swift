//
//  MembershipModel.swift
//  Wagen
//
//  Created by top Dev on 25.11.2020.
//

import Foundation
import SwiftyJSON

class MembershipModel: NSObject {
    
    var id : String?
    var delete_status : String?
    var max_order_per_month : String?
    var price : String?
    var title : String?

    override init() {
       super.init()
        id = ""
        delete_status = ""
        max_order_per_month = ""
        price = ""
        title = ""
    }
    
    init(_ json : JSON) {
        self.id = json["id"].stringValue
        self.delete_status = json["delete_status"].stringValue
        self.max_order_per_month = json["max_order_per_month"].stringValue
        self.price = json["price"].stringValue
        self.title = json["title"].stringValue
    }
}

class MembershipCell: UITableViewCell {
    
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_price: UILabel!
    @IBOutlet weak var btn_perchae: UIButton!
    
    func setMemberships(_ entity: MembershipModel!) {
        lbl_title.text = entity.title
        lbl_price.text = "Precio: $" + splitWithDot(entity.price!)
    }
}

class PurchasedMembershipCell: UITableViewCell {
    
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_price: UILabel!
    @IBOutlet weak var lbl_purchased_at: UILabel!
    @IBOutlet weak var lbl_order_available: UILabel!
    @IBOutlet weak var cons_height: NSLayoutConstraint!
    
    func setMemberships(_ entity: MembershipModel!, purchasedat: String?, orderavailable: String?) {
        lbl_title.text = entity.title
        lbl_price.text = "Precio: $" + splitWithDot(entity.price!)
        if let purchased = purchasedat, let orderavailable = orderavailable{
            lbl_purchased_at.text = "Pagado: " + purchased
            lbl_order_available.text = "Reserva Disponible: " + orderavailable
        }
    }
}
