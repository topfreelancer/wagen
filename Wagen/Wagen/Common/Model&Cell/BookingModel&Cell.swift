//
//  NotiModel&Cell.swift
//  emoglass
//
//  Created by Mac on 7/8/20.
//  Copyright © 2020 Mac. All rights reserved.
//


import Foundation
import Kingfisher

class BookingModel{
    
    var id : String?
    var date: String?
    var time: String?
    var duration: String?
    var price: String?
    
    init(id: String,date: String, time: String, duration: String, price: String) {
        self.id = id
        self.time = time
        self.date = date
        self.duration = duration
        self.price = price
    }
}


class MyBookingCell: UITableViewCell {
    
    @IBOutlet weak var lbl_date: UILabel!
    @IBOutlet weak var lbl_time: UILabel!
    @IBOutlet weak var lbl_duration: UILabel!
    @IBOutlet weak var lbl_price: UILabel!
    
    func setBooking(_ entity: BookingModel!) {
        lbl_date.text = "Fecha de Reserva: " + entity.date!
        lbl_time.text = "Hora de Reserva: " + entity.time!
        lbl_duration.text = "Duración: " + entity.duration! + "MINS"
        lbl_price.text = "Precio Total: " + splitWithDot(entity.price!) + "CLP"
    }
    
    func setReview(_ entity: ReviewModel!) {
        lbl_date.text = "Fecha de Reserva: " + entity.date!
        lbl_time.text = "Hora de Reserva: " + entity.time!
        lbl_duration.text = "Duración: " + entity.duration! + "MINS"
        lbl_price.text = "Precio Total: " + splitWithDot(entity.price!) + "CLP"
    }
}




