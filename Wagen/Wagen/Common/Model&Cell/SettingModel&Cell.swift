//
//  SettingModel.swift
//  EveraveUpdate
//
//  Created by Ubuntu on 12/18/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import Foundation
import UIKit

class SettingModel {
    
    var settingCaption:String = ""
    
    init (_ settingCaption:String)
    {
        self.settingCaption = settingCaption
    }
}


class SettingCell: UITableViewCell {
    
    
    @IBOutlet weak var setting_lbl: UILabel!
    
    var entity : SettingModel!{
        
        didSet{
            setting_lbl.text = entity.settingCaption
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected{
            contentView.backgroundColor = UIColor.black.withAlphaComponent(0)
        }
        else{
            contentView.backgroundColor = UIColor.black.withAlphaComponent(0)
        }
    }
}
