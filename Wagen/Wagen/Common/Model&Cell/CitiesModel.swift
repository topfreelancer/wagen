//
//  CitiesModel.swift
//  Wagen
//
//  Created by top Dev on 25.11.2020.
//


import Foundation
import SwiftyJSON

class CitiesModel: NSObject {
    
    var id : String?
    var city_name : String?
    var delete_status : String?
    var order_limit : String?

    override init() {
       super.init()
        id = ""
        city_name = ""
        delete_status = ""
        order_limit = ""
    }
    
    init(_ json : JSON) {
        self.id = json["id"].stringValue
        self.city_name = json["city_name"].stringValue
        self.delete_status = json["delete_status"].stringValue
        self.order_limit = json["order_limit"].stringValue
    }
}
