//
//  ServicesModel.swift
//  Wagen
//
//  Created by top Dev on 25.11.2020.
//

import Foundation
import SwiftyJSON

class ServicesModel: NSObject {
    
    var service_id : String?
    var cu_status : String?
    var service_delete_status : String?
    var service_description : String?
    var service_name : String?
    var service_price : String?
    var service_time : String?

    override init() {
       super.init()
        service_id = ""
        cu_status = ""
        service_delete_status = ""
        service_description = ""
        service_name = ""
        service_price = ""
        service_time = ""
    }
    
    init(_ json : JSON) {
        self.service_id = json["service_id"].stringValue
        self.cu_status = json["cu_status"].stringValue
        self.service_delete_status = json["service_delete_status"].stringValue
        self.service_description = json["service_description"].stringValue
        self.service_name = json["service_name"].stringValue
        self.service_price = json["service_price"].stringValue
        self.service_time = json["service_time"].stringValue
    }
}
