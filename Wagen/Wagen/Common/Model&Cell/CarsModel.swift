//
//  CarsModel.swift
//  Wagen
//
//  Created by top Dev on 25.11.2020.
//

import Foundation
import SwiftyJSON

class CarsModel: NSObject {
    
    var car_id : String?
    var car_delete_status : String?
    var car_name : String?
    var car_photo : String?

    override init() {
       super.init()
        car_id = ""
        car_delete_status = ""
        car_name = ""
        car_photo = ""
    }
    
    init(_ json : JSON) {
        self.car_id = json["car_id"].stringValue
        self.car_delete_status = json["car_delete_status"].stringValue
        self.car_name = json["car_name"].stringValue
        self.car_photo = json["car_photo"].stringValue
    }
}
