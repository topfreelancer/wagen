//
//  CouponCodes.swift
//  Wagen
//
//  Created by top Dev on 25.11.2020.
//

import Foundation
import SwiftyJSON

class CouponCodes: NSObject {
    
    var id : String?
    var delete_status : String?
    var code : String?
    var discount_value : String?
    var end_date : String?
    var start_date : String?

    override init() {
       super.init()
        id = ""
        delete_status = ""
        code = ""
        discount_value = ""
        end_date = ""
        start_date = ""
    }
    
    init(_ json : JSON) {
        self.id = json["id"].stringValue
        self.delete_status = json["delete_status"].stringValue
        self.code = json["code"].stringValue
        self.discount_value = json["discount_value"].stringValue
        self.end_date = json["end_date"].stringValue
        self.start_date = json["start_date"].stringValue
    }
}
