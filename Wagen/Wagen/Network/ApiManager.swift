//
//  ApiManager.swift
//  Everave Update
//
//  Created by Ubuntu on 16/01/2020
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import Alamofire
import Foundation
import SwiftyJSON
// ************************************************************************//
                            // Wagen project //
// ************************************************************************//

let SERVER_URL = "https://servicioswagen.cl/admin/index.php/Api/"
//let SERVER_URL = "http://192.168.101.33:8080/CarwashBackend/index.php/Api/"   // local server url

let PAYMENT_URL = "https://wagen.cl/admin/index.php/PaymentApi"


let LOGIN = SERVER_URL + "login"
let REGISTER_USER = SERVER_URL + "registerUser"
let GETALL_MYORDER = SERVER_URL + "getallmyorder"
let GETALL_AVAILABLETIME = SERVER_URL + "getdateavailabletime"
let PURCHASE_MEMBERSHIP = SERVER_URL + "purchasemembership"
let CREATE_ORDER = SERVER_URL + "createorder"
let UPDATE_PASSWORD = SERVER_URL + "updatepassword"
let UPDATE_PROFILE = SERVER_URL + "updateprofile"
let UPLOAD_IMAGE = SERVER_URL + "uploadImage"
let FORGOT_CONFIRM_EMAIL = SERVER_URL + "forgotconfirmemail"
let GETALL_MY_REVIEW_LIST = SERVER_URL + "getallmynotreviewdjobs"
let GIVE_REVIEW = SERVER_URL + "giveorderreivew"

struct PARAMS {
    static let MESSAGE    = "message"
    static let SUCCESS    = "success"
    static let IS_CHEKCED = "is_checked"
}

class ApiManager {
    
    class func parseTotalInfo(_ json: JSON){
        self.parseUserInfo(json)
        cars.removeAll()
        packages.removeAll()
        promotions.removeAll()
        services.removeAll()
        cities.removeAll()
        memberships.removeAll()
        workshop.removeAll()
        couponcodes.removeAll()
        weburls = WeburlModel()
        if let carss = json["cars"].arrayObject{
            for one in carss {
                cars.append(CarsModel(JSON(one)))
            }
        }
        if let citiess = json["cities"].arrayObject{
            for one in citiess {
                cities.append(CitiesModel(JSON(one)))
            }
        }
        if let couponcodess = json["couponcodes"].arrayObject{
            for one in couponcodess {
                couponcodes.append(CouponCodes(JSON(one)))
            }
        }
        if let membershipss = json["memberships"].arrayObject{
            for one in membershipss {
                memberships.append(MembershipModel(JSON(one)))
            }
        }
        if let packagess = json["packages"].arrayObject{
            for one in packagess {
                packages.append(PackagesModel(JSON(one)))
            }
        }
        if let promotionss = json["promotions"].arrayObject{
            for one in promotionss {
                promotions.append(PromotionModel(JSON(one)))
            }
        }
        if let servicess = json["services"].arrayObject{
            for one in servicess {
                services.append(ServicesModel(JSON(one)))
            }
        }
        if let workshops = json["workshop"].arrayObject{
            for one in workshops {
                workshop.append(WorkshopModel(JSON(one)))
            }
        }
        weburls = WeburlModel(JSON(json["weburls"].object))
        print(weburls)
    }
    
    class func parseUserInfo(_ json: JSON){
        thisuser!.clearUserInfo()
        thisuser = UserModel(JSON(json["user_info"].object))
        thisuser!.saveUserInfo()
        thisuser!.loadUserInfo()
    }
    

    class func login(email : String, password: String, account_type: String, completion :  @escaping (_ success: Bool, _ response : String?) -> ()) {
        // account type: 0: general, 1: google, 2: facebook
        let params = ["email" : email, "password": password, "account_type": account_type] as [String : Any]
        Alamofire.request(LOGIN, method:.post, parameters:params)
        .responseJSON { response in
            switch response.result {
                case .failure:
                completion(false, nil)
                case .success(let data):
                let dict = JSON(data)
                //print("userinformation =====> ",dict)
                let status = dict[PARAMS.MESSAGE].stringValue
                if status == PARAMS.SUCCESS {
                    self.parseTotalInfo(dict)
                    completion(true, status)
                } else {
                    completion(false, status)
                }
            }
        }
    }
    
    class func socialLogin(firstname: String, lastname: String, email : String, account_type: String, photo: String, completion :  @escaping (_ success: Bool, _ response : String?) -> ()) {
        // account type: 0: general, 1: google, 2: facebook
        let params = ["first_name" : firstname,"last_name" : lastname,"email" : email, "password": "social", "photo": photo,"account_type": account_type,"social_id": "", "phonenumber": ""] as [String : Any]
        Alamofire.request(SERVER_URL + "loginonloginpage", method:.post, parameters:params)
        .responseJSON { response in
            switch response.result {
                case .failure:
                completion(false, nil)
                case .success(let data):
                let dict = JSON(data)
                //print("userinformation =====> ",dict)
                let status = dict[PARAMS.MESSAGE].stringValue
                if status == PARAMS.SUCCESS {
                    self.parseTotalInfo(dict)
                    completion(true, status)
                } else {
                    completion(false, status)
                }
            }
        }
    }
    
    class func applelogin(email: String, first_name: String,last_name: String, social_id: String, completion :  @escaping (_ success: Bool, _ response : String?) -> ()) {
        let params = ["first_name": first_name, "last_name": last_name, "email": email,  "social_id": social_id ] as [String : Any]
        Alamofire.request(SERVER_URL + "applelogin", method:.post, parameters:params)
        .responseJSON { response in
            switch response.result {
                case .failure:
                completion(false, nil)
                case .success(let data):
                let dict = JSON(data)
                let message = dict[PARAMS.MESSAGE].stringValue
                if message == PARAMS.SUCCESS {
                    self.parseTotalInfo(dict)
                    completion(true,message)
                } else {
                    completion(false, message)
                }
            }
        }
    }
    
    
    class func registerUser(first_name: String, last_name: String,email: String, phonenumber: String,password: String, photo: String,account_type: String, social_id: String, completion :  @escaping (_ success: Bool, _ response : String?) -> ()) {
        let params = ["first_name": first_name, "last_name": last_name, "email": email, "phonenumber": phonenumber, "password": password, "photo": photo, "account_type": account_type, "social_id": social_id, ] as [String : Any]
        Alamofire.request(REGISTER_USER, method:.post, parameters:params)
        .responseJSON { response in
            switch response.result {
                case .failure:
                completion(false, nil)
                case .success(let data):
                let dict = JSON(data)
                //print("sellers =====> ",dict)
                let message = dict[PARAMS.MESSAGE].stringValue
                if message == PARAMS.SUCCESS {
                    self.parseTotalInfo(dict)
                    completion(true,message)
                } else {
                    completion(false, message)
                }
            }
        }
    }
    
    
    
    class func getallmyorder(completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        // account type: 0: general, 1: google, 2: facebook
        let params = ["user_id" : thisuser!.user_id ?? "0"] as [String : Any]
        Alamofire.request(GETALL_MYORDER, method:.post, parameters:params)
        .responseJSON { response in
            switch response.result {
                case .failure:
                completion(false, nil)
                case .success(let data):
                let dict = JSON(data)
                //print("userinformation =====> ",dict)
                let status = dict[PARAMS.MESSAGE].stringValue
                if status == PARAMS.SUCCESS {
                    completion(true, dict)
                } else {
                    completion(false, status)
                }
            }
        }
    }
    
    class func getdateavailabletime(city : String,date : String,order_type : String, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        // account type: 0: general, 1: google, 2: facebook
        let params = ["city" : city, "date" : date, "order_type" : order_type] as [String : Any]
        Alamofire.request(GETALL_AVAILABLETIME, method:.post, parameters:params)
        .responseJSON { response in
            switch response.result {
                case .failure:
                completion(false, nil)
                case .success(let data):
                let dict = JSON(data)
                print("thisistimeinformation =====> ",dict)
                let status = dict[PARAMS.MESSAGE].stringValue
                if status == PARAMS.SUCCESS {
                    completion(true, dict)
                } else {
                    completion(false, status)
                }
            }
        }
    }
    
    class func purchasemembership(membershipid : String,memership_count : String, completion :  @escaping (_ success: Bool, _ renewdate : String?, _ remaining_order : String?) -> ()) {
        // account type: 0: general, 1: google, 2: facebook
        let params = ["userid" : thisuser!.user_id ?? "0", "membershipid" : membershipid, "memership_count" : memership_count] as [String : Any]
        Alamofire.request(PURCHASE_MEMBERSHIP, method:.post, parameters:params)
        .responseJSON { response in
            switch response.result {
                case .failure:
                completion(false, nil, nil)
                case .success(let data):
                let dict = JSON(data)
                //print("userinformation =====> ",dict)
                let status = dict[PARAMS.MESSAGE].stringValue
                if status == PARAMS.SUCCESS {
                    completion(true, dict["renewdate"].stringValue, dict["remaining_order"].stringValue)
                } else {
                    completion(false, status, status)
                }
            }
        }
    }
    
    class func createorder(order_date : String,order_time : String,duration_time : String,total_price : String,car_id : String,package_id : String,service_ids : String,order_type : String,order_city : String,order_address : String,payment_method : String,service_qty : String,coupon_id : String,discount_percent : String,paid_price : String,completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        // account type: 0: general, 1: google, 2: facebook
        let params = ["user_id" : thisuser!.user_id ?? "0", "order_date" : order_date, "order_time" : order_time, "duration_time" : duration_time, "total_price" : total_price, "car_id" : car_id, "package_id" : package_id, "service_ids" : service_ids, "order_type" : order_type, "order_city" : order_city, "order_address" : order_address, "payment_method" : payment_method, "service_qty" : service_qty, "coupon_id" : coupon_id, "discount_percent" : discount_percent, "paid_price" : paid_price] as [String : Any]
        Alamofire.request(CREATE_ORDER, method:.post, parameters:params)
        .responseJSON { response in
            switch response.result {
                case .failure:
                completion(false, nil)
                case .success(let data):
                let dict = JSON(data)
                //print("userinformation =====> ",dict)
                let status = dict[PARAMS.MESSAGE].stringValue
                let order_id = dict["order_id"].stringValue
                if status == PARAMS.SUCCESS {
                    completion(true, order_id)
                } else {
                    completion(false, status)
                }
            }
        }
    }
    
    class func updatepassword(email : String,password : String, completion :  @escaping (_ success: Bool) -> ()) {
        // account type: 0: general, 1: google, 2: facebook
        let params = ["email" : email, "password" : password] as [String : Any]
        Alamofire.request(UPDATE_PASSWORD, method:.post, parameters:params)
        .responseJSON { response in
            switch response.result {
                case .failure:
                completion(false)
                case .success(let data):
                let dict = JSON(data)
                //print("userinformation =====> ",dict)
                let status = dict[PARAMS.MESSAGE].stringValue
                if status == PARAMS.SUCCESS {
                    completion(true)
                } else {
                    completion(false)
                }
            }
        }
    }
    
    class func updateprofile( first_name : String,last_name : String, email : String, phonenumber : String,photo : String,phone_verifystatus : String, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        // account type: 0: general, 1: google, 2: facebook
        let params = ["user_id" : thisuser!.user_id ?? "0", "first_name" : first_name, "last_name" : last_name, "email" : email, "phonenumber" : phonenumber, "password" : thisuser!.password ?? "", "photo" : photo, "phone_verifystatus" : phone_verifystatus] as [String : Any]
        Alamofire.request(UPDATE_PROFILE, method:.post, parameters:params)
        .responseJSON { response in
            switch response.result {
                case .failure:
                completion(false, nil)
                case .success(let data):
                let dict = JSON(data)
                //print("userinformation =====> ",dict)
                let status = dict[PARAMS.MESSAGE].stringValue
                if status == PARAMS.SUCCESS {
                    self.parseUserInfo(dict)
                    completion(true, status)
                } else {
                    completion(false, status)
                }
            }
        }
    }
    
    class func uploadImage(image: String, completion :  @escaping (_ success: Bool, _ response : String?) -> ()) {
        let requestURL = UPLOAD_IMAGE
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                if !image.isEmpty{
                    multipartFormData.append(URL(fileURLWithPath: image), withName: "image")
                }
            },
            to: requestURL,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                    case .success(let upload, _, _):
                    upload.responseJSON { response in
                        switch response.result {
                            case .failure: completion(false, nil)
                            case .success(let data):
                            let dict = JSON(data)
                            let status = dict[PARAMS.MESSAGE].stringValue
                            if status == PARAMS.SUCCESS {
                                let image_url = dict["image_url"].stringValue
                                completion(true, image_url)
                            } else{
                                completion(false, status)
                            }
                        }
                    }
                    case .failure( _):
                    completion(false, nil)
                }
            }
        )
    }
    
    class func forgotconfirmemail(email : String, completion :  @escaping (_ success: Bool, _ response : String?) -> ()) {
        let params = ["email" : email] as [String : Any]
        Alamofire.request(FORGOT_CONFIRM_EMAIL, method:.post, parameters:params)
        .responseJSON { response in
            switch response.result {
                case .failure:
                completion(false, nil)
                case .success(let data):
                let dict = JSON(data)
                //print("userinformation =====> ",dict)
                let status = dict[PARAMS.MESSAGE].stringValue
                if status == PARAMS.SUCCESS {
                    let verificationcode = dict["verificationcode"].stringValue
                    completion(true, verificationcode)
                } else {
                    completion(false, status)
                }
            }
        }
    }
    
    class func getallMyReviewList(completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = ["user_id" : thisuser!.user_id ?? "0"] as [String : Any]
        Alamofire.request(GETALL_MY_REVIEW_LIST, method:.post, parameters:params)
        .responseJSON { response in
            switch response.result {
                case .failure:
                completion(false, nil)
                case .success(let data):
                let dict = JSON(data)
                //print("userinformation =====> ",dict)
                let status = dict[PARAMS.MESSAGE].stringValue
                if status == PARAMS.SUCCESS {
                    completion(true, dict)
                } else {
                    completion(false, status)
                }
            }
        }
    }
    
    class func giveReview(order_id : String,rating : String,comment : String,completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = ["order_id" : order_id, "rating":rating, "comment":comment] as [String : Any]
        Alamofire.request(GIVE_REVIEW, method:.post, parameters:params)
        .responseJSON { response in
            switch response.result {
                case .failure:
                completion(false, nil)
                case .success(let data):
                let dict = JSON(data)
                //print("userinformation =====> ",dict)
                let status = dict[PARAMS.MESSAGE].stringValue
                if status == PARAMS.SUCCESS {
                    completion(true, status)
                } else {
                    completion(false, status)
                }
            }
        }
    }
    
    class func createPaymentUrl(_ orderamount: String) -> String {
        //http://192.168.101.33:8080/CarwashBackend/index.php/Payment
        //+ "?orderid="+Constants.orderid+"&amount="+Constants.orderamount
        //Constants.userModel.first_name+ts;
        let returnedurl = PAYMENT_URL + "?orderid=" + thisuser!.first_name! + "\(Int(NSDate().timeIntervalSince1970) * 1000)" + "&amount=" + orderamount
        return returnedurl
    }
    
    class func getPreferenceId(amount: String, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = ["user_email" : thisuser!.email ?? "0", "total_price": amount] as [String : Any]
        Alamofire.request("https://servicioswagen.cl/admin/index.php/MercagoPayment/index", method:.post, parameters:params)
        .responseJSON { response in
            switch response.result {
                case .failure:
                completion(false, nil)
                case .success(let data):
                let dict = JSON(data)
                //print("userinformation =====> ",dict)
                let status = dict[PARAMS.MESSAGE].stringValue
                if status == PARAMS.SUCCESS {
                    completion(true, dict)
                } else {
                    completion(false, status)
                }
            }
        }
    }
}

