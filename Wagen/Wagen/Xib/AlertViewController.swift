//
//  AlertViewController.swift
//  NBIBottomSheet_Example
//
//  Created by Bichon, Nicolas on 2018-10-02.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import UIKit

class AlertViewController: UIViewController {

    // MARK: - View Life Cycle
    var flag = -1
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        view.roundCorners([.topLeft, .topRight], radius: 4)
    }

    // MARK: - Actions

    @IBAction func dismissButtonTapped(sender: AnyObject?) {
        dismiss(animated: true) {
            var dataDict = [String:Any]()
            dataDict.removeAll()
            if self.flag == 0{
                dataDict = ["is_delivery": false]
            }else if self.flag == 1{
                dataDict = ["is_delivery": true]
            }
            NotificationCenter.default.post(name:.showAddress, object: nil, userInfo: dataDict)
        }
    }
}

