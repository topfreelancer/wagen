//
//  RatingViewController.swift
//  PopupDialog
//
//  Created by Martin Wildfeuer on 11.07.16.
//  Copyright © 2016 CocoaPods. All rights reserved.
//

import UIKit

class RatingViewController: BaseVC {
    
    var content: String?
    var titlee: String?
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_content: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        if let content = content{
            lbl_content.text = content
        }
        if let titlee = titlee{
            lbl_title.text = titlee
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeBtnClicked(_ sender: Any) {
        self.dismissModal()
    }
    
    @IBAction func backBtnClicked(_ sender: Any) {
        self.dismissModal()
    }
    
    func dismissModal() {
        self.gotoVC(VCs.HOME_NAV)
    }
}

