//
//  EditProfileVC.swift
//  Wagen
//
//  Created by top Dev on 10/24/20.
//

import UIKit
import SKCountryPicker

class EditProfileVC: BaseVC, UITextFieldDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    @IBOutlet weak var countryCode: UILabel!
    @IBOutlet weak var countryName: UILabel!
    @IBOutlet weak var cons_contryName: NSLayoutConstraint!
    @IBOutlet weak var edt_first_name: UITextField!
    @IBOutlet weak var edt_last_name: UITextField!
    @IBOutlet weak var edt_email: UITextField!
    @IBOutlet weak var edt_phone_number: UITextField!
    @IBOutlet weak var imv_avatar: UIImageView!
    
    var imagePicker: ImagePicker1!
    var first_name = ""
    var last_name = ""
    var email = ""
    var phone_number = ""
    var image_path = thisuser!.photo ?? ""
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
    }
    
    func setUI() {
        self.title = "Editar Perfil"
        editInit()
        addleftButton()
        imv_avatar.addTapGesture(tapNumber: 1, target: self, action: #selector(onEdtPhoto))
        self.imagePicker = ImagePicker1(presentationController: self, delegate: self)
        
        let imageURL = thisuser!.photo
        if let url = URL(string:imageURL ?? ""){
            imv_avatar.kf.setImage(with: url,placeholder: UIImage(named: "ic_avatar"))
        }
        
        self.edt_first_name.text = thisuser!.first_name
        self.edt_last_name.text = thisuser!.last_name
        self.edt_email.text = thisuser!.email
        self.edt_phone_number.text = thisuser!.phonenumber
        let titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        UISegmentedControl.appearance().setTitleTextAttributes(titleTextAttributes, for: .selected)
    }
    
    @objc func onEdtPhoto(gesture: UITapGestureRecognizer) -> Void {
        self.imagePicker.present(from: view)
    }
    
    func gotoUploadProfile(_ image: UIImage?) {
        self.image_path = ""
        if let image = image{
            let imagepath = saveToFile(image:image,filePath:"photo",fileName:randomString(length: 2)) // Image save action
            if let image_path = imagepath{
                self.showLoadingView(vc: self)
                ApiManager.uploadImage(image: image_path) { (isSuccess, image_url) in
                    self.hideLoadingView()
                    if isSuccess{
                        if let imageurl = image_url{
                            self.image_path = imageurl
                            print(self.image_path)
                            DispatchQueue.main.async {
                                self.imv_avatar.image = image
                            }
                        }
                    }else{
                        if let imageurl = image_url{
                            //self.showToast(imageurl)
                            print(imageurl)
                        }
                    }
                }
            }
        }
    }
    
    func editInit() {
        setEdtPlaceholder(edt_first_name, placeholderText: "", placeColor: UIColor.lightGray, padding: .left(10))
        setEdtPlaceholder(edt_last_name, placeholderText: "", placeColor: UIColor.lightGray, padding: .left(10))
        setEdtPlaceholder(edt_email, placeholderText: "", placeColor: UIColor.lightGray, padding: .left(10))
        setEdtPlaceholder(edt_phone_number, placeholderText: "", placeColor: UIColor.lightGray, padding: .left(50))
    }
    
    func addleftButton() {
        let btn_back = UIButton(type: .custom)
        btn_back.setImage(UIImage (named: "ic_arrow_left")!.withRenderingMode(.alwaysTemplate), for: .normal)
        btn_back.addTarget(self, action: #selector(gotoHome), for: .touchUpInside)
        btn_back.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        btn_back.tintColor = UIColor.white
        
        let barButtonItemBack = UIBarButtonItem(customView: btn_back)
        barButtonItemBack.customView?.widthAnchor.constraint(equalToConstant: 20).isActive = true
        barButtonItemBack.customView?.heightAnchor.constraint(equalToConstant: 25).isActive = true
        self.navigationItem.leftBarButtonItem = barButtonItemBack
    }
    
    @objc func gotoHome() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func countryCodeClicked(_ sender: Any) {
        let countryController = CountryPickerWithSectionViewController.presentController(on: self) { [weak self] (country: Country) in

            guard let self = self else { return }

            self.countryCode.text = country.dialingCode!
            self.countryName.text = country.countryName
            self.cons_contryName.constant = CGFloat(country.countryName.count * 8)
        }
        // can customize the countryPicker here e.g font and color
        countryController.detailColor = UIColor.red
        CountryManager.shared.addFilter(.countryCode)
        CountryManager.shared.addFilter(.countryDialCode)
    }
    
    @IBAction func saveBtnClicked(_ sender: Any) {
        first_name = self.edt_first_name.text ?? ""
        last_name = self.edt_last_name.text ?? ""
        email = self.edt_email.text ?? ""
        phone_number = self.edt_phone_number.text ?? ""
        /*if self.image_path.isEmpty{
            self.showToast(Messages.ADD_PHOTO)
            return
        }*/
        if first_name.isEmpty{
            self.showToast(Messages.FIRST_NAME_REQUIRE)
            return
        }
        if last_name.isEmpty{
            self.showToast(Messages.LAST_NAME_REQUIRE)
            return
        }
        if email.isEmpty{
            self.showToast(Messages.EMAIL_REQUIRE)
            return
        }
        if !email.isValidEmail(){
            self.showToast(Messages.VALID_EMAIL_REQUIRE)
            return
        }
        if phone_number.isEmpty{
            self.showToast(Messages.PHONE_NUMBER_REQUIRE)
            return
        }
        else{
            self.showLoadingView(vc: self)
            ApiManager.updateprofile(first_name: first_name, last_name: last_name, email: email, phonenumber: phone_number, photo: image_path, phone_verifystatus: "1") { (isSuccess, data) in
                self.hideLoadingView()
                if isSuccess{
                    self.showCustomDialog(titlee: "Message", content: "Los cambios en tu perfil fueron realizados")
                }else{
                    self.showToast(Messages.NETISSUE)
                }
            }
        }
    }
}

extension EditProfileVC: ImagePickerDelegate1{
    
    func didSelect(image: UIImage?) {
        self.gotoUploadProfile(image)
    }
}
