//
//  CalendarVC.swift
//  Wagen
//
//  Created by top Dev on 10/25/20.
//

import UIKit
import SwiftyJSON

class CalendarVC: BaseVC {

    @IBOutlet weak var calendarViewV: CalendarView!
    var heredate: Date? = nil
    @IBOutlet weak var col_timesShow: UICollectionView!
    @IBOutlet weak var cons_col_timesShowHeight: NSLayoutConstraint!
    @IBOutlet weak var lbl_intermediate: UILabel!
    var selected_city: String? = "José Alcalde Délano 10.581, Lo Barnechea 23."
    var order_type: String? = "0"

    var timesList = [timeModel]()
    var booktime: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initUI()
        setCalendarView()
    }
    
    func initUI() {
        self.lbl_intermediate.text = "Tipo de\nlavado"
        self.setTitle("Fecha y Hora")
        self.addleftButton()
    }
    
    func addleftButton() {
        let btn_back = UIButton(type: .custom)
        btn_back.setImage(UIImage (named: "ic_arrow_left")!.withRenderingMode(.alwaysTemplate), for: .normal)
        btn_back.addTarget(self, action: #selector(gotoHome), for: .touchUpInside)
        btn_back.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        btn_back.tintColor = UIColor.white
        
        let barButtonItemBack = UIBarButtonItem(customView: btn_back)
        barButtonItemBack.customView?.widthAnchor.constraint(equalToConstant: 20).isActive = true
        barButtonItemBack.customView?.heightAnchor.constraint(equalToConstant: 25).isActive = true
        self.navigationItem.leftBarButtonItem = barButtonItemBack
    }
    
    @objc func gotoHome() {
        self.navigationController?.popViewController(animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setTimeList(Date())
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let today = Date()
        var tomorrowComponents = DateComponents()
        tomorrowComponents.day = 0 // selected date setting
        #if KDCALENDAR_EVENT_MANAGER_ENABLED
        self.calendarViewV.loadEvents() { error in
            if error != nil {
                let message = "The karmadust calender could not load system events. It is possibly a problem with permissions"
                let alert = UIAlertController(title: "Events Loading Error", message: message, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
        #endif
        
        if today.dayofTheWeek != ""{
            print("This is the weekday number===>,\(today.dayofTheWeek)")
        }
        self.calendarViewV.setDisplayDate(today)
    }
    
    func setCalendarView()  {
        let style = CalendarView.Style()
        
        style.cellShape                = .round
        style.cellColorDefault         = UIColor.clear
        style.cellColorToday           = UIColor.init(named: "color_primary")!
        style.cellSelectedBorderColor  = UIColor.init(named: "color_primary")!
        style.cellEventColor           = UIColor.clear
        style.headerTextColor          = UIColor.darkGray
        style.cellTextColorDefault     = UIColor.black
        style.cellTextColorToday       = UIColor.white
        style.cellTextColorWeekend     = UIColor.black
        //style.cellColorOutOfRange      = UIColor(red: 249/255, green: 226/255, blue: 212/255, alpha: 1.0)
        style.cellColorOutOfRange      = .lightGray
        style.headerBackgroundColor    = UIColor.init(named: "color_edt_back")!
        style.weekdaysBackgroundColor  = UIColor.init(named: "color_edt_back")!
        style.firstWeekday             = .sunday
        //style.locale                   = Locale(identifier: Constants.localTimeZoneIdentifier)
        
        style.locale                   = NSLocale(localeIdentifier: "es_CL") as Locale
        
        style.cellFont = UIFont(name: "Helvetica", size: 18.0) ?? UIFont.systemFont(ofSize: 18.0)
        style.headerFont = UIFont(name: "Helvetica", size: 18.0) ?? UIFont.systemFont(ofSize: 18.0)
        style.weekdaysFont = UIFont(name: "Helvetica", size: 12.0) ?? UIFont.systemFont(ofSize: 12.0)
        calendarViewV.style = style
        calendarViewV.dataSource = self
        calendarViewV.delegate = self
        calendarViewV.direction = .horizontal
        calendarViewV.multipleSelectionEnable = false
        calendarViewV.marksWeekends = true
        calendarViewV.backgroundColor = UIColor.init(named: "color_edt_back")
    }
    
    func setTimeList(_ date: Date) {
        print(date)
        main_service_summary.datee = self.getDateStringFromDate(date: date)
        self.showLoadingView(vc: self)
        ApiManager.getdateavailabletime(city: self.selected_city ?? "", date: self.getDateStringFromDate(date: date), order_type: self.order_type ?? "") { (isSuccess, data) in
            self.hideLoadingView()
            let dict = JSON(data as Any)
            if let times = dict["useabletime"].arrayObject{
                self.timesList.removeAll()
                if times.count > 0{
                    var num = 0
                    for one in times{
                        num += 1
                        self.timesList.append(timeModel(one as? String))
                    }
                    if num == times.count{
                        self.col_timesShow.reloadData()
                        
                        var rows = 0
                        if num % 2 == 0{
                            rows = num / 2
                        }else{
                            rows = (num + 1) / 2
                        }
                        
                        if rows == 0{
                            self.cons_col_timesShowHeight.constant = 60
                        }else{
                            self.cons_col_timesShowHeight.constant = CGFloat(rows * 40) + 20
                        }
                    }
                }else{
                    self.cons_col_timesShowHeight.constant = 0
                }
            }
        }
    }
    
    func getDateStringFromDate(date: Date, format: String = "yyyy-MM-dd") -> String {
        let df = DateFormatter()
        df.dateFormat = format
        return df.string(from: date)
    }
    
    @IBAction func nextBtnClicked(_ sender: Any) {
        if self.booktime.isEmpty{
            self.showToast("Por favor selecciona una hora")
        }else{
            main_service_summary.timee = booktime
            self.gotoNavPresent(VCs.BOOKING_SUMMARY, fullscreen: true, animated: true)
        }
    }
}

extension CalendarVC: CalendarViewDataSource {
    // MARK:here we can add start limited day
    func startDate() -> Date {
        var dateComponents = DateComponents()
        dateComponents.year = 0
        let seconds = TimeZone.current.secondsFromGMT()
        print("seconds =====> ",seconds)
        let hour = seconds / 3600
        dateComponents.hour = hour
        let today = Date()
        let threeMonthsAgo = self.calendarViewV.calendar.date(byAdding: dateComponents, to: today)! // here can set the start date
        return threeMonthsAgo
    }
    // MARK: here we can add start limited day
    func endDate() -> Date {
        var dateComponents = DateComponents()
        dateComponents.month = 1200
        let today = Date()
        let twoYearsFromNow = self.calendarViewV.calendar.date(byAdding: dateComponents, to: today)!
        return twoYearsFromNow
    }
}

extension CalendarVC: CalendarViewDelegate {
    
    func calendar(_ calendar: CalendarView, didSelectDate date : Date, withEvents events: [CalendarEvent]) {
        heredate = date
        //print("Did Select: \(date) with \(events.count) events")
        setTimeList(date)
    }

    func calendar(_ calendar: CalendarView, didScrollToMonth date : Date) {
        print(self.calendarViewV.selectedDates)
        //self.datePicker.setDate(date, animated: true)
    }
}

//MARK: UICollectionViewDataSource
extension CalendarVC: UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return timesList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "timeCell", for: indexPath) as! timeCell
        cell.entity = timesList[indexPath.row]
        return cell
    }
}

extension CalendarVC: UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        booktime = timesList[indexPath.row].str_time ?? ""
    }
}

extension CalendarVC: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w = collectionView.frame.size.width / 2.1
        return CGSize(width: w, height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
            return 0
        }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
}

