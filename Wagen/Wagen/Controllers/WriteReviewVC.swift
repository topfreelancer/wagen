//
//  WriteReviewVC.swift
//  Wagen
//
//  Created by top Dev on 11/4/20.
//

import UIKit
import Kingfisher
import Cosmos

class WriteReviewVC: BaseVC, UITextViewDelegate{

    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var tbv_my_bookings: UITableView!
    @IBOutlet weak var imv_carphoto: UIImageView!
    @IBOutlet weak var lbl_carname: UILabel!
    @IBOutlet weak var lbl_packagename: UILabel!
    @IBOutlet weak var lbl_packageprice: UILabel!
    @IBOutlet weak var lbl_packageduration: UILabel!
    @IBOutlet weak var lbl_packagecontent: UILabel!
    @IBOutlet weak var cons_tblheight: NSLayoutConstraint!
    @IBOutlet weak var cus_review: CosmosView!
    @IBOutlet weak var uiv_package: UIView!
    var selected_review = ReviewModel()
    var ds_mybookings = [ServicesModel]()
    var isRequesting = false
    let cellSpacingHeight: CGFloat = 20
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addleftButton()
        self.title = "Resumen de Reserva"
        textView.text = "Write your review"
        textView.textColor = UIColor.darkGray
        textView.textContainerInset = UIEdgeInsets(top: 10, left: 10, bottom: 0, right: 0)
        textView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.isRequesting = false
        getDataSource()
        setUI()
    }
    
    func getDataSource() {
        
    }
    
    func setUI() {
        tbv_my_bookings.rowHeight = UITableView.automaticDimension
        tbv_my_bookings.estimatedRowHeight = 80
        let url = URL(string: selected_review.car_photo ?? "" )
        if let url = url{
            KingfisherManager.shared.retrieveImage(with: url, options: nil, progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
                self.imv_carphoto.image = image?.withRenderingMode(.alwaysTemplate)
            })
        }
        
        lbl_carname.text = selected_review.car_name
        for one in packages{
            if one.package_id == self.selected_review.package_id{
                self.lbl_packagename.text = one.package_name
                self.lbl_packageduration.text = one.package_time! + "MINS"
                if let content = one.package_description{
                    self.lbl_packagecontent.text = content.replacingOccurrences(of: "_", with: "\n", options: .literal, range: nil)
                }
                for two in one.package_prices{
                    if two.car_id == self.selected_review.car_id{
                        if self.selected_review.order_type == "0"{
                            self.lbl_packageprice.text = splitWithDot(two.price!) + "CLP"
                        }else{
                            self.lbl_packageprice.text = splitWithDot(two.price_home!) + "CLP"
                        }
                    }
                }
            }
        }
        self.ds_mybookings = self.selected_review.services ?? []
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.cons_tblheight.constant = tbv_my_bookings.contentSize.height
    }
    
    func addleftButton() {
        let btn_back = UIButton(type: .custom)
        btn_back.setImage(UIImage (named: "ic_arrow_left")!.withRenderingMode(.alwaysTemplate), for: .normal)
        btn_back.addTarget(self, action: #selector(gotoHome), for: .touchUpInside)
        btn_back.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        btn_back.tintColor = UIColor.white
        
        let barButtonItemBack = UIBarButtonItem(customView: btn_back)
        barButtonItemBack.customView?.widthAnchor.constraint(equalToConstant: 20).isActive = true
        barButtonItemBack.customView?.heightAnchor.constraint(equalToConstant: 25).isActive = true
        self.navigationItem.leftBarButtonItem = barButtonItemBack
    }
    
    @objc func gotoHome() {
        //self.navigationController?.popViewController(animated: true)
        self.gotoVC(VCs.HOME_NAV)
    }
    
    @IBAction func reviewBtnClicked(_ sender: Any) {
        self.showLoadingView(vc: self)
        ApiManager.giveReview(order_id: self.selected_review.id ?? "0", rating: "\(self.cus_review.rating)", comment: self.textView.text) { (isSuccess, data) in
            self.hideLoadingView()
            if isSuccess{
                self.showCustomDialog(titlee: "Message", content: "Gracias por tu evaluación, esto nos ayuda a mejorar cada día!")
            }else{
                self.showToast(Messages.NETISSUE)
            }
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.darkGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Write your review"
            textView.textColor = UIColor.darkGray
        }
    }
}

extension WriteReviewVC : UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.ds_mybookings.count
    }
    
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tbv_my_bookings?.dequeueReusableCell(withIdentifier: "ReviewCell", for:indexPath) as! ReviewCell
        cell.selectionStyle = .none
        cell.setReview(self.ds_mybookings[indexPath.section].service_name ?? "", price: ds_mybookings[indexPath.section].service_price ?? "", duration: ds_mybookings[indexPath.section].service_time ?? "")
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return cellSpacingHeight
    }
}
