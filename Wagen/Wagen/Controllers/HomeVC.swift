//
//  HomeVC.swift
//  Wagen
//
//  Created by top Dev on 10/24/20.
//

import UIKit
import ImageSlideshow
import Alamofire

class HomeVC: BaseVC {

    @IBOutlet weak var uiv_book_service: dropShadowView!
    @IBOutlet weak var imv_bookservice: UIImageView!
    @IBOutlet weak var lbl_book_service: UILabel!
    @IBOutlet weak var uiv_my_bookings: dropShadowView!
    @IBOutlet weak var imv_my_bookings: UIImageView!
    @IBOutlet weak var lbl_my_bookings: UILabel!
    @IBOutlet weak var uiv_my_profile: dropShadowView!
    @IBOutlet weak var imv_my_profile: UIImageView!
    @IBOutlet weak var lbl_my_profile: UILabel!
    @IBOutlet weak var uiv_settings: dropShadowView!
    @IBOutlet weak var imv_settings: UIImageView!
    @IBOutlet weak var lbl_settings: UILabel!
    @IBOutlet weak var uiv_give_review: dropShadowView!
    @IBOutlet weak var imv_give_review: UIImageView!
    @IBOutlet weak var lbl_give_review: UILabel!
    @IBOutlet weak var slideshow: ImageSlideshow!
    var imageData = [InputSource]()
    var currentpage = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        uiv_book_service.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tap(_:))))
        uiv_book_service.isUserInteractionEnabled = true
        uiv_book_service.tag = ServiceType.book_service.rawValue
        uiv_my_bookings.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tap(_:))))
        uiv_my_bookings.isUserInteractionEnabled = true
        uiv_my_bookings.tag = ServiceType.my_bookings.rawValue
        uiv_my_profile.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tap(_:))))
        uiv_my_profile.isUserInteractionEnabled = true
        uiv_my_profile.tag = ServiceType.my_profile.rawValue
        uiv_settings.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tap(_:))))
        uiv_settings.isUserInteractionEnabled = true
        uiv_settings.tag = ServiceType.settings.rawValue
        uiv_give_review.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tap(_:))))
        uiv_give_review.isUserInteractionEnabled = true
        uiv_give_review.tag = ServiceType.give_review.rawValue
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showNavBar()
        initUI()
        setUI(.null)
    }
    
    func initUI() {
        self.title = "Inicio"
        /*let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes*/
        slideShowInit()
        addRightBtn()
    }
    
    func addRightBtn() {
        let btn_back = UIButton(type: .custom)
        btn_back.setImage(UIImage (named: "more")!.withRenderingMode(.alwaysTemplate), for: .normal)
        btn_back.addTarget(self, action: #selector(moreBtnClicked), for: .touchUpInside)
        btn_back.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        btn_back.tintColor = UIColor.white
        let barButtonItemBack = UIBarButtonItem(customView: btn_back)
        barButtonItemBack.customView?.widthAnchor.constraint(equalToConstant: 35).isActive = true
        barButtonItemBack.customView?.heightAnchor.constraint(equalToConstant: 35).isActive = true
        self.navigationItem.rightBarButtonItem = barButtonItemBack
    }
    
    @objc func moreBtnClicked() {
        /*self.showAboutDialog(titlee: "¿Qué es WAGEN?", content: "• Somos una empresa que ofrece servicio premium de lavado a mano de autos a domicilio ubicados en el sector oriente de Santiago.\n\n• Constantemente generamos contenido informativo que le entregue consejos a nuestros clientes y/o audiencia con respecto al cuidado y mantención de sus vehículos.\n\n• Utilizamos productos ecológicos siempre preocupándonos por el cuidado de nuestro entorno.\n\n• Buscamos la excelencia por sobre todo.\n\n• Estamos ubicados en José Alcalde Délano 10.581, Lo Barnechea.")*/
        self.gotoWebViewWithProgressBar(weburls.introl_url ?? "")
    }
    
    func showAboutDialog(titlee: String, content: String) {
        let ratingVC = AboutViewController(nibName: "About", bundle: nil)
        ratingVC.titlee = titlee
        ratingVC.content = content
        ratingVC.modalPresentationStyle = .overFullScreen
        self.present(ratingVC, animated: false, completion: nil)
    }
    
    func slideShowInit()  {
        slideshow.slideshowInterval = 2.5
        slideshow.pageIndicatorPosition = .init(horizontal: .center, vertical: .under)
        slideshow.contentScaleMode = UIViewContentMode.scaleAspectFill
        setIndicator(slideshow, "banner")
        if #available(iOS 13.0, *) {
            slideshow.activityIndicator = DefaultActivityIndicator(style: .medium, color: nil)
        } else {
            // Fallback on earlier versions
        }
        slideshow.delegate = self
        self.imageData.removeAll()
        for one in promotions{
            if let url = one.banner_image{
                if let image = KingfisherSource(urlString: url){
                    self.imageData.append(image)
                }
            }
        }
        //print(promotions)
        /*for i in 0 ... 2{
            self.imageData.append(KingfisherSource(urlString: "https://www.crushpixel.com/static13/preview2/stock-photo-flash-sale-banner-promotion-template-design-on-red-background-with-golden-thunder-big-sale-special-60-offer-labels-end-of-season-special-offer-banner-shop-now-1224771.jpg")!)
        }*/
        
        slideshow.setImageInputs(imageData)
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTap))
        slideshow.addGestureRecognizer(gestureRecognizer)
    }
    
    @objc func didTap(_ index : Int) {
        print(self.currentpage)
      //slideshow.presentFullScreenController(from: self)
        self.gotoNavPresent("SliderShowVC", fullscreen: true, animated: true)
    }
    
    func setIndicator (_ imgSlider: ImageSlideshow, _ type: String) {
        let pageControl = UIPageControl()
        pageControl.currentPageIndicatorTintColor = UIColor.init(named: "color_primary")
        pageControl.pageIndicatorTintColor = .lightGray
        imgSlider.pageIndicator = pageControl
        imgSlider.pageIndicatorPosition = PageIndicatorPosition(horizontal: .center, vertical: .bottom)
    }
    
    @objc func tap(_ gestureRecognizer: UITapGestureRecognizer) {
        let tag = gestureRecognizer.view?.tag
        switch tag! {
        case ServiceType.book_service.rawValue :
            setUI(.book_service)
            presentVCs(VCs.SELECT_VEHICLE)
        case ServiceType.my_bookings.rawValue :
            setUI(.my_bookings)
            presentVCs(VCs.MY_BOOKING)
        case ServiceType.my_profile.rawValue :
            setUI(.my_profile)
            presentVCs(VCs.EDIT_PROFILE)
        case ServiceType.settings.rawValue :
            setUI(.settings)
            presentVCs(VCs.SETTINGS)
        case ServiceType.give_review.rawValue :
            setUI(.give_review)
            presentVCs(VCs.GIVE_REVIEW)
        default:
            print("default")
        }
    }
    
    func presentVCs(_ vcname: String) {
        self.gotoNavPresent(vcname, fullscreen: true,animated: true)
    }
    
    func setUI(_ index: ServiceType){
        
        if index == .book_service{
            self.setColor(true, view: self.uiv_book_service, imv: imv_bookservice, lbl: lbl_book_service)
            self.setColor(false, view: self.uiv_my_bookings, imv: imv_my_bookings, lbl: lbl_my_bookings)
            self.setColor(false, view: self.uiv_my_profile, imv: imv_my_profile, lbl: lbl_my_profile)
            self.setColor(false, view: self.uiv_settings, imv: imv_settings, lbl: lbl_settings)
            self.setColor(false, view: self.uiv_give_review, imv: imv_give_review, lbl: lbl_give_review)
        }else if index == .my_bookings{
            self.setColor(false, view: self.uiv_book_service, imv: imv_bookservice, lbl: lbl_book_service)
            self.setColor(true, view: self.uiv_my_bookings, imv: imv_my_bookings, lbl: lbl_my_bookings)
            self.setColor(false, view: self.uiv_my_profile, imv: imv_my_profile, lbl: lbl_my_profile)
            self.setColor(false, view: self.uiv_settings, imv: imv_settings, lbl: lbl_settings)
            self.setColor(false, view: self.uiv_give_review, imv: imv_give_review, lbl: lbl_give_review)
        }else if index == .my_profile{
            self.setColor(false, view: self.uiv_book_service, imv: imv_bookservice, lbl: lbl_book_service)
            self.setColor(false, view: self.uiv_my_bookings, imv: imv_my_bookings, lbl: lbl_my_bookings)
            self.setColor(true, view: self.uiv_my_profile, imv: imv_my_profile, lbl: lbl_my_profile)
            self.setColor(false, view: self.uiv_settings, imv: imv_settings, lbl: lbl_settings)
            self.setColor(false, view: self.uiv_give_review, imv: imv_give_review, lbl: lbl_give_review)
        }else if index == .settings{
            self.setColor(false, view: self.uiv_book_service, imv: imv_bookservice, lbl: lbl_book_service)
            self.setColor(false, view: self.uiv_my_bookings, imv: imv_my_bookings, lbl: lbl_my_bookings)
            self.setColor(false, view: self.uiv_my_profile, imv: imv_my_profile, lbl: lbl_my_profile)
            self.setColor(true, view: self.uiv_settings, imv: imv_settings, lbl: lbl_settings)
            self.setColor(false, view: self.uiv_give_review, imv: imv_give_review, lbl: lbl_give_review)
        }else if index == .give_review{
            self.setColor(false, view: self.uiv_book_service, imv: imv_bookservice, lbl: lbl_book_service)
            self.setColor(false, view: self.uiv_my_bookings, imv: imv_my_bookings, lbl: lbl_my_bookings)
            self.setColor(false, view: self.uiv_my_profile, imv: imv_my_profile, lbl: lbl_my_profile)
            self.setColor(false, view: self.uiv_settings, imv: imv_settings, lbl: lbl_settings)
            self.setColor(true, view: self.uiv_give_review, imv: imv_give_review, lbl: lbl_give_review)
        }else{
            self.setColor(false, view: self.uiv_book_service, imv: imv_bookservice, lbl: lbl_book_service)
            self.setColor(false, view: self.uiv_my_bookings, imv: imv_my_bookings, lbl: lbl_my_bookings)
            self.setColor(false, view: self.uiv_my_profile, imv: imv_my_profile, lbl: lbl_my_profile)
            self.setColor(false, view: self.uiv_settings, imv: imv_settings, lbl: lbl_settings)
            self.setColor(false, view: self.uiv_give_review, imv: imv_give_review, lbl: lbl_give_review)
        }
    }
    
    func setColor(_ selected: Bool, view: UIView, imv: UIImageView, lbl: UILabel) {
        if selected{
            view.backgroundColor = UIColor.init(named: "color_primary")
            imv.tintColor = .white
            lbl.textColor = .white
        }else{
            view.backgroundColor = UIColor.init(named: "color_edt_back")
            imv.tintColor = .darkGray
            lbl.textColor = .darkGray
        }
    }
    @IBAction func membershiipBtnClicked(_ sender: Any) {
        self.gotoNavPresent(VCs.MEMBERSHIP, fullscreen: true, animated: true)
    }
}

extension HomeVC: ImageSlideshowDelegate{
    func imageSlideshow(_ imageSlideshow: ImageSlideshow, didChangeCurrentPageTo page: Int) {
        self.currentpage = page
    }
}

enum ServiceType: Int {
    case book_service
    case my_bookings
    case my_profile
    case settings
    case give_review
    case null
}


