//
//  WashPackagesVC.swift
//  Wagen
//
//  Created by top Dev on 10/25/20.
//

import UIKit

class WashPackagesVC: BaseVC{

    @IBOutlet weak var tbv_wash_packages: UITableView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var cus_lookingfor: MSDropDown!
    @IBOutlet weak var cus_fixedAddress: MSDropDown!
    @IBOutlet weak var uiv_dlgback: UIView!
    @IBOutlet weak var uiv_dialog: UIView!
    @IBOutlet weak var edt_address: UITextField!
    @IBOutlet weak var uiv_address: UIView!
    @IBOutlet weak var lbl_intermediate: UILabel!
    var carid = 0
    var ds_services = [WashPackageModel]()
    var ds_services_total = [WashPackageModel]()
    var isRequesting = false
    let cellSpacingHeight: CGFloat = 20
    var location_type = 1
    var city = cities.first?.city_name
    var fixed_address = workshop.first?.name
    var lookingfor_Options = [KeyValueModel]()
    var fixed_address_options = [KeyValueModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //toVC?.animatedItems[3].badgeValue = nil
        //badgeVal = nil
        //self.tabBarController?.delegate = self
        //toVC?.animatedItems[3].badgeValue = nil
        NotificationCenter.default.addObserver(self, selector: #selector(showAddress(_:)),name:.showAddress, object: nil)
        setUI()
        tbv_wash_packages.rowHeight = UITableView.automaticDimension
        tbv_wash_packages.estimatedRowHeight = 80
        segmentedControl.addTarget(self, action: #selector(WashPackagesVC.indexChanged(_:)), for: .valueChanged)
        let titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        UISegmentedControl.appearance().setTitleTextAttributes(titleTextAttributes, for: .selected)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.isRequesting = false
        getDataSource()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if self.hud != nil{
            if hud!.isFocused{
                self.hideLoadingView()
            }
        }
    }

    @objc func showAddress(_ notification: NSNotification){
        if let dict = notification.userInfo as NSDictionary?, let is_delivery = dict["is_delivery"] as? Bool{
            if is_delivery{// delivery state
                self.setDropDown()
            }else{// fix state
                showFixedAddressDropDown()
            }
        }
    }
    
    @IBAction func uiviewBackBtnClicked(_ sender: Any) {
        for one in self.ds_services{
            one.selected = false
        }
        self.tbv_wash_packages.reloadData()
        dropDownVisible(show: false)
        setAddress(show: false)
    }
    
    func dropDownVisible(show: Bool) {
        if show{
            self.uiv_dlgback.isHidden = false
            self.uiv_dialog.isHidden = false
        }else{
            self.edt_address.text? = ""
            self.uiv_dlgback.isHidden = true
            self.uiv_dialog.isHidden = true
        }
    }
    
    func setAddress(show: Bool) {// for fixded location
        if show{
            self.uiv_dlgback.isHidden = false
            self.uiv_address.isHidden = false
            /*self.lbl_address.text = "José Alcalde Délano 10.581,\nLo Barnechea."*/
        }else{
            self.uiv_dlgback.isHidden = true
            self.uiv_address.isHidden = true
        }
    }
    
    func setDropDown(){
        dropDownVisible(show: true)
        self.edt_address.addPadding(.left(4))
        var num = 0
        self.lookingfor_Options.removeAll()
        for one in cities{
            num += 1
            lookingfor_Options.append(KeyValueModel(key: one.id ?? "", value: one.city_name ?? ""))
            if num == cities.count{
                self.cus_lookingfor.keyvalueCount = self.lookingfor_Options.count
                self.cus_lookingfor.delegate = self
                self.cus_lookingfor.keyValues = self.lookingfor_Options
                self.cus_lookingfor.placeholder = self.lookingfor_Options.first?.value ?? ""
                self.cus_lookingfor.tag = AddressType.delivery.rawValue
                self.cus_lookingfor.isMultiSelect = false
            }
        }
    }
    
    
    func showFixedAddressDropDown(){
        setAddress(show: true)
        self.fixed_address_options.removeAll()
        var num = 0
        for one in workshop{
            num += 1
            fixed_address_options.append(KeyValueModel(key: one.id ?? "", value: one.name ?? ""))
            if num == workshop.count{
                self.cus_fixedAddress.keyvalueCount = self.fixed_address_options.count
                self.cus_fixedAddress.delegate = self
                self.cus_fixedAddress.keyValues = self.fixed_address_options
                self.cus_fixedAddress.placeholder = self.fixed_address_options.first?.value ?? ""
                self.cus_lookingfor.tag = AddressType.fixed.rawValue
                self.cus_fixedAddress.isMultiSelect = false
            }
        }
    }
    
    func setUI() {
        self.lbl_intermediate.text = "Tipo de\nlavado"
        dropDownVisible(show: false)
        setAddress(show: false)
        self.addleftButton()
        self.title = "Packs de Servicios"
        tbv_wash_packages.tableFooterView = UIView()
    }
    
    func addleftButton() {
        let btn_back = UIButton(type: .custom)
        btn_back.setImage(UIImage (named: "ic_arrow_left")!.withRenderingMode(.alwaysTemplate), for: .normal)
        btn_back.addTarget(self, action: #selector(gotoHome), for: .touchUpInside)
        btn_back.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        btn_back.tintColor = UIColor.white
        
        let barButtonItemBack = UIBarButtonItem(customView: btn_back)
        barButtonItemBack.customView?.widthAnchor.constraint(equalToConstant: 20).isActive = true
        barButtonItemBack.customView?.heightAnchor.constraint(equalToConstant: 25).isActive = true
        self.navigationItem.leftBarButtonItem = barButtonItemBack
    }
    
    @objc func gotoHome() {
        self.navigationController?.popViewController(animated: false)
    }
    
    func getDataSource()  {
        self.ds_services.removeAll()
        self.ds_services_total.removeAll()
        var num = 0
        for i in 0 ... packages.count  - 1{
            num += 1
            for two in packages[i].package_prices{
                if two.car_id == "\(self.carid)"{
                    self.ds_services_total.append(WashPackageModel(id: packages[i].package_id ?? "", service_type: packages[i].package_name ?? "", price: packages[i].package_prices[self.carid - 1].price ?? "", price_home: packages[i].package_prices[self.carid - 1].price_home ?? "", duration: packages[i].package_time ?? "", service_content: packages[i].package_description ?? "", selected: false, package_available_for_home: packages[i].package_available_for_home == "Yes" ? true : false))
                }
            }
            
            if num == packages.count{
                //dump(self.ds_services)
                
                self.location_type = 1
                var num = 0
                for one in ds_services_total{
                    num += 1
                    if one.package_available_for_home {
                        self.ds_services.append(one)
                    }
                    if num == ds_services.count{
                        self.tbv_wash_packages.reloadData()
                    }
                }
            }
        }
    }
    @IBAction func bookBtnClicked(_ sender: Any) {
        let button = sender as! UIButton
        let index = button.tag
        self.ds_services[index].selected = true
        for i in 0 ... ds_services.count - 1{
            if i != index{
                ds_services[i].selected = false
            }
        }
        self.tbv_wash_packages.reloadData()
        if self.location_type == 1{// delivery flag = 1
            self.showBottomSheetControllerWithFlag(1)
        }else{// fixed location flag = 0
            self.showBottomSheetControllerWithFlag(0)
        }
    }
    
    func showBottomSheetControllerWithFlag(_ flag: Int)  {
        
        for one in ds_services{
            if one.selected{
                if flag == 1{
                    main_service_summary.price = one.price_home
                }else{
                    main_service_summary.price = one.price
                }
                main_service_summary.duration = one.duration
                main_service_summary.package_id = one.id
            }
        }
        var configuration: NBBottomSheetConfiguration
        let viewController = AlertViewController()
        configuration = NBBottomSheetConfiguration(sheetSize: .fixed(80))
        let bottomSheetController = NBBottomSheetController(configuration: configuration)
        viewController.flag = flag
        bottomSheetController.present(viewController, on: self)
    }
    
    @IBAction func okBtnClicked(_ sender: Any) {// fixed price goto additional service vc
        if let address = self.edt_address.text{
            if address.isEmpty{
                self.showToast("Ingresar Dirección")
            }else{
                for one in self.ds_services{
                    one.selected = false
                }
                self.tbv_wash_packages.reloadData()
                dropDownVisible(show: false)
                main_service_summary.order_type = OrderType.homeorder
                // next
                main_service_summary.order_city = self.city
                main_service_summary.address = address
                self.gotoNavPresent(VCs.ADDITIONAL_SERVICE, fullscreen: true, animated: false)
            }
        }
    }
    
    @IBAction func addressNextBtnClicked(_ sender: Any) {
        // goto additional service with custom home address
        self.showFixedAddressDropDown()
        main_service_summary.order_type = OrderType.workshop
        main_service_summary.address = self.fixed_address
        self.gotoNavPresent(VCs.ADDITIONAL_SERVICE, fullscreen: true, animated: false)
    }
    
    @objc func indexChanged(_ sender: UISegmentedControl) {
        for one in ds_services{
            one.selected = false
        }
        if segmentedControl.selectedSegmentIndex == 0 {
            self.location_type = 0
            self.ds_services.removeAll()
            self.ds_services = self.ds_services_total
            self.tbv_wash_packages.reloadData()
        } else if segmentedControl.selectedSegmentIndex == 1 {
            self.location_type = 1
            self.ds_services.removeAll()
            var num = 0
            for one in ds_services_total{
                num += 1
                if one.package_available_for_home {
                    self.ds_services.append(one)
                }
                if num == ds_services.count{
                    self.tbv_wash_packages.reloadData()
                }
            }
        }
    }
}

extension WashPackagesVC : UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.ds_services.count
    }
    
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tbv_wash_packages?.dequeueReusableCell(withIdentifier: "WashPackageCell", for:indexPath) as! WashPackageCell
        cell.selectionStyle = .none
        //cell.entity = ds_services[indexPath.section]
        cell.setEntiy(self.location_type, entity: ds_services[indexPath.section])
        cell.btn_book.tag = indexPath.section
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let index = indexPath.section
        self.ds_services[index].selected = true
        for i in 0 ... ds_services.count - 1{
            if i != index{
                ds_services[i].selected = false
            }
        }
        self.tbv_wash_packages.reloadData()
        if self.location_type == 1{
            self.showBottomSheetControllerWithFlag(1)
        }else{
            self.showBottomSheetControllerWithFlag(0)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return cellSpacingHeight
    }
}

extension WashPackagesVC: MSDropDownDelegate{
    func dropdownSelected(tagId: Int, answer: String, value: String, isSelected: Bool) {
        print(answer)
        switch tagId {
        case AddressType.delivery.rawValue:
            self.city = answer
        case AddressType.fixed.rawValue:
            self.fixed_address = answer
        default:
            print("")
        }
    }
}

enum AddressType: Int {
    case delivery
    case fixed
}





