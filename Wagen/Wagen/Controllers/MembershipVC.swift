//
//  MembershipVC.swift
//  Wagen
//
//  Created by top Dev on 11/4/20.
//

import UIKit
import StoreKit
import MercadoPagoSDK
import SwiftyJSON

class MembershipVC: BaseVC {

    @IBOutlet weak var tbv_my_bookings: UITableView!
    var ds_mybookings = [MembershipModel]()
    let cellSpacingHeight: CGFloat = 20
    var price: Int?
    var products: [SKProduct] = []
    var membership_model: MembershipModel?
    
    private var checkout: MercadoPagoCheckout?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUI()
        initPurchase()
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.tbv_my_bookings.estimatedRowHeight = 100
        getDataSource()
        self.price = nil
        self.membership_model = nil
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tbv_my_bookings.reloadData()
    }
    
    func initPurchase() {
        IAPHandler.shared.fetchAvailableProducts()
        IAPHandler.shared.purchaseStatusBlock = {[weak self] (type) in
            //self?.hideLoadingView()
            guard let strongSelf = self else{ return }
            if type == .purchased {
                let alertView = UIAlertController(title: "Wagen", message: type.message(), preferredStyle: .alert)
                let action = UIAlertAction(title: "Okay", style: .default, handler: { (alert) in
                    if let memberhipmodel = self!.membership_model{
                        self!.paymentProcess(membership: memberhipmodel)
                    }
                })
                alertView.addAction(action)
                strongSelf.present(alertView, animated: true, completion: nil)
            
            } else if type == .restored {
                
                let alertView = UIAlertController(title: "Wagen", message: type.message(), preferredStyle: .alert)
                let action = UIAlertAction(title: "Okay", style: .default, handler: { (alert) in
                    //TODO: success purchased
                    print("restore succeed")
                })
                alertView.addAction(action)
                strongSelf.present(alertView, animated: true, completion: nil)
            } else {
                print(type.message())
                if let message = type.message(){
                    self!.showToast(message)
                }
            }
        }
    }
    
    func getDataSource() {
        self.ds_mybookings = memberships
    }
    
    func setUI()  {
        self.title = "Membresía"
        self.addleftButton()
    }
    
    func addleftButton() {
        let btn_back = UIButton(type: .custom)
        btn_back.setImage(UIImage (named: "ic_arrow_left")!.withRenderingMode(.alwaysTemplate), for: .normal)
        btn_back.addTarget(self, action: #selector(gotoHome), for: .touchUpInside)
        btn_back.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        btn_back.tintColor = UIColor.white
        
        let barButtonItemBack = UIBarButtonItem(customView: btn_back)
        barButtonItemBack.customView?.widthAnchor.constraint(equalToConstant: 20).isActive = true
        barButtonItemBack.customView?.heightAnchor.constraint(equalToConstant: 25).isActive = true
        self.navigationItem.leftBarButtonItem = barButtonItemBack
    }
    
    @objc func gotoHome() {
        //self.navigationController?.popViewController(animated: true)
        self.gotoVC(VCs.HOME_NAV)
    }
    
    
    public func presentAlert(from sourceView: UIView, index: Int) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        if let action = self.action(title: "Paga con Apple Pay", index: index) {
            alertController.addAction(action)
        }
        if let action = self.action(title: "Paga con tarjeta", index: index) {
            alertController.addAction(action)
        }
        alertController.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
        self.present(alertController, animated: true)
    }
    
    private func action(title: String, index: Int) -> UIAlertAction? {
        return UIAlertAction(title: title, style: .default) { [unowned self] _ in
            self.membership_model = self.ds_mybookings[index]
            if title == "Paga con Apple Pay"{// apple pay
                if let price = self.ds_mybookings[index].price?.toInt(){
                    print(price)
                    self.price = price
                    if price >= 79900{
                        IAPHandler.shared.purchaseMyProduct(strProductID: WagenProducts.wagen_membership_maitenace4)
                    }else{
                        IAPHandler.shared.purchaseMyProduct(strProductID: WagenProducts.wagen_membership_maitenace2)
                    }
                }
            }else{// direct pay
                if let price = self.ds_mybookings[index].price{
                    self.showLoadingView(vc: self)
                    ApiManager.getPreferenceId(amount: price) { (isSuccess, data) in
                        self.hideLoadingView()
                        if isSuccess{
                            let dict = JSON(data as Any)
                            //****************** local payment ***************//
                            let builder = MercadoPagoCheckoutBuilder(publicKey: Constants.MercadoPublicKey, preferenceId:dict["preferenceid"].stringValue).setLanguage("es")
                            self.checkout = MercadoPagoCheckout(builder: builder)
                            // Start with your navigation controller.
                            if let myNavigationController = self.navigationController {
                                self.checkout?.start(navigationController: myNavigationController, lifeCycleProtocol: self)
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    @IBAction func purchaseBtnClicked(_ sender: Any) {
        // setting apple pay and local payment process separately
        //*************** apple pay ********************//
        // starting apple pay
        pay_type = .membership_pay
        let button = sender as! UIButton
        let indexx = button.tag
        self.presentAlert(from: self.view, index: indexx)
    }
    
    func paymentProcess(membership: MembershipModel) {
        ApiManager.purchasemembership(membershipid: membership.id ?? "1", memership_count: membership.max_order_per_month ?? "") { (isSuccess1, renewdate, remaining) in
            if isSuccess1{
                UserDefault.setString(key: "membership_id", value: membership.id ?? "1")
                UserDefault.setString(key: "membership_last_renew", value: renewdate)
                UserDefault.setString(key: "membership_count", value: remaining)
                UserDefault.Sync()
                thisuser!.loadUserInfo()
                thisuser!.saveUserInfo()
                self.tbv_my_bookings.reloadData()
            }else{
                self.showAlerMessage(message: Messages.NETISSUE)
            }
        }
    }
}

extension MembershipVC : UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.ds_mybookings.count
    }
    
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if thisuser?.membership_count == "0"{
            let cell = tbv_my_bookings?.dequeueReusableCell(withIdentifier: "MembershipCell", for:indexPath) as! MembershipCell
            cell.selectionStyle = .none
            cell.setMemberships(ds_mybookings[indexPath.section])
            cell.btn_perchae.tag = indexPath.section
            return cell
        }else{
            let cell = tbv_my_bookings?.dequeueReusableCell(withIdentifier: "PurchasedMembershipCell", for:indexPath) as! PurchasedMembershipCell
            cell.selectionStyle = .none
            if thisuser?.membership_id == ds_mybookings[indexPath.section].id{
                cell.setMemberships(ds_mybookings[indexPath.section], purchasedat: thisuser?.membership_last_renew, orderavailable: thisuser?.membership_count)
            }else{
                cell.setMemberships(ds_mybookings[indexPath.section], purchasedat: nil, orderavailable: nil)
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //return Constants.SCREEN_HEIGHT / 10
        /*if thisuser?.membership_count == "0"{
            return 150
        }else{
            
            /*if thisuser?.membership_id == ds_mybookings[indexPath.section].id{
                return 160
            }else{
                return 100
            }*/
            return UITableView.automaticDimension
        }*/
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return cellSpacingHeight
    }
}

extension MembershipVC :PXLifeCycleProtocol{
    func cancelCheckout() -> (() -> Void)? {
        return {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func finishCheckout() -> ((PXResult?) -> Void)? {
        return  ({ (_ payment: PXResult?) in
            //print(payment)
            if let payment  = payment{
                if payment.getStatus() == "approved"{
                    if let memberhipmodel = self.membership_model{
                        //self.showLoadingView(vc: self)
                        ApiManager.purchasemembership(membershipid: memberhipmodel.id ?? "1", memership_count: memberhipmodel.max_order_per_month ?? "") { (isSuccess1, renewdate, remaining) in
                            //self.hideLoadingView()
                            if isSuccess1{
                                UserDefault.setString(key: "membership_id", value: memberhipmodel.id ?? "1")
                                UserDefault.setString(key: "membership_last_renew", value: renewdate)
                                UserDefault.setString(key: "membership_count", value: remaining)
                                UserDefault.Sync()
                                thisuser!.loadUserInfo()
                                thisuser!.saveUserInfo()
                                self.navigationController?.popToRootViewController(animated: true)
                                self.tbv_my_bookings.reloadData()
                            }else{
                                self.showAlerMessage(message: Messages.NETISSUE)
                            }
                        }
                    }
                }
            }else{
                self.navigationController?.popViewController(animated: true)
            }
        })
    }
}

