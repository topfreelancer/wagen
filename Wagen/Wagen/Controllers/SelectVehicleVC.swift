//
//  SelectVehicleVC.swift
//  Wagen
//
//  Created by top Dev on 10/24/20.
//

import UIKit
import Kingfisher

class SelectVehicleVC: BaseVC {

    @IBOutlet weak var uiv_book_service: dropShadowView!
    @IBOutlet weak var imv_bookservice: UIImageView!// sedan
    @IBOutlet weak var lbl_book_service: UILabel!// sedan label
    @IBOutlet weak var uiv_my_bookings: dropShadowView!
    @IBOutlet weak var imv_my_bookings: UIImageView!// hackback
    @IBOutlet weak var lbl_my_bookings: UILabel!// hackback label
    @IBOutlet weak var uiv_my_profile: dropShadowView!
    @IBOutlet weak var imv_my_profile: UIImageView!// minvan
    @IBOutlet weak var lbl_my_profile: UILabel!// minivan label
    @IBOutlet weak var uiv_settings: dropShadowView!
    @IBOutlet weak var imv_settings: UIImageView!// suv
    @IBOutlet weak var lbl_settings: UILabel!// suv label
    @IBOutlet weak var uiv_give_review: dropShadowView!
    @IBOutlet weak var imv_give_review: UIImageView!// camioneta
    @IBOutlet weak var lbl_give_review: UILabel!// camioneta label
    @IBOutlet weak var lbl_intermedicate: UILabel!
    var selected_vehicle: VehicleType = .null
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI(.null)
        uiv_book_service.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tap(_:))))
        uiv_book_service.isUserInteractionEnabled = true
        uiv_book_service.tag = ServiceType.book_service.rawValue
        uiv_my_bookings.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tap(_:))))
        uiv_my_bookings.isUserInteractionEnabled = true
        uiv_my_bookings.tag = ServiceType.my_bookings.rawValue
        uiv_my_profile.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tap(_:))))
        uiv_my_profile.isUserInteractionEnabled = true
        uiv_my_profile.tag = ServiceType.my_profile.rawValue
        uiv_settings.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tap(_:))))
        uiv_settings.isUserInteractionEnabled = true
        uiv_settings.tag = ServiceType.settings.rawValue
        uiv_give_review.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tap(_:))))
        uiv_give_review.isUserInteractionEnabled = true
        uiv_give_review.tag = ServiceType.give_review.rawValue
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showNavBar()
        initUI()
    }
    
    func initUI() {
        self.title = "Seleccione el Tipo de Vehículo"
        addleftButton()
        self.setVehicleOptions(imv_bookservice, label: lbl_book_service, index: 0)
        self.setVehicleOptions(imv_my_bookings, label: lbl_my_bookings, index: 1)
        self.setVehicleOptions(imv_my_profile, label: lbl_my_profile, index: 2)
        self.setVehicleOptions(imv_settings, label: lbl_settings, index: 3)
        self.setVehicleOptions(imv_give_review, label: lbl_give_review, index: 4)
    }
    
    func setVehicleOptions(_ imv: UIImageView, label: UILabel, index: Int)  {
        if let carurl = cars[index].car_photo{
            if let url = URL(string: carurl){
                //imv.kf.setImage(with: url,placeholder: UIImage.init(named: "placeholder"))
                KingfisherManager.shared.retrieveImage(with: url, options: nil, progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
                    imv.image = image?.withRenderingMode(.alwaysTemplate)
                })
            }
        }
        if let carname = cars[index].car_name{
            label.text = carname
        }
    }
    
    
    func addleftButton() {
        let btn_back = UIButton(type: .custom)
        btn_back.setImage(UIImage (named: "ic_arrow_left")!.withRenderingMode(.alwaysTemplate), for: .normal)
        btn_back.addTarget(self, action: #selector(gotoHome), for: .touchUpInside)
        btn_back.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        btn_back.tintColor = UIColor.white
        
        let barButtonItemBack = UIBarButtonItem(customView: btn_back)
        barButtonItemBack.customView?.widthAnchor.constraint(equalToConstant: 20).isActive = true
        barButtonItemBack.customView?.heightAnchor.constraint(equalToConstant: 25).isActive = true
        self.navigationItem.leftBarButtonItem = barButtonItemBack
    }
    
    @objc func gotoHome() {
        //self.navigationController?.popViewController(animated: true)
        self.gotoVC(VCs.HOME_NAV)
    }
    
    @objc func tap(_ gestureRecognizer: UITapGestureRecognizer) {
        let tag = gestureRecognizer.view?.tag
        switch tag! {
        case ServiceType.book_service.rawValue :
            setUI(.book_service)
            self.selected_vehicle = .sedan
        case ServiceType.my_bookings.rawValue :
            setUI(.my_bookings)
            self.selected_vehicle = .hatchback
        case ServiceType.my_profile.rawValue :
            setUI(.my_profile)
            self.selected_vehicle = .minivan
        case ServiceType.settings.rawValue :
            setUI(.settings)
            self.selected_vehicle = .suv
        case ServiceType.give_review.rawValue :
            setUI(.give_review)
            self.selected_vehicle = .camioneta
        default:
            print("default")
        }
    }
    
    @IBAction func nextBtnClicked(_ sender: Any) {
        // CHECK PHONE NUMBER
        let number = thisuser?.phonenumber ?? ""
        let email = thisuser?.email ?? ""
        if number.isEmpty || email.isEmpty{
            let alertController = UIAlertController(title: nil, message: Messages.ADD_PHONE_NUMBER_EMAIL, preferredStyle: .alert)
            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                self.gotoNavPresent(VCs.EDIT_PROFILE, fullscreen: true, animated: true)
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .default) { (action:UIAlertAction!) in
            }
            alertController.addAction(OKAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion:nil)
        }else{
            if self.selected_vehicle != .null{
                main_service_summary = SummaryModel()
                let packvc = self.createVC(VCs.WASH_PACKAGES) as! WashPackagesVC
                if self.selected_vehicle == .sedan{
                    packvc.carid = 1
                }else if self.selected_vehicle == .hatchback{
                    packvc.carid = 2
                }else if self.selected_vehicle == .minivan{
                    packvc.carid = 3
                }else if self.selected_vehicle == .suv{
                    packvc.carid = 4
                }else if self.selected_vehicle == .camioneta{
                    packvc.carid = 5
                }
                main_service_summary.car_id = "\(packvc.carid)"
                self.gotoNavPresentWithVC(packvc, fullscreen: true)
            }else{
                self.showToast(Messages.SELECT_VEHICLE)
            }
        }
    }
    
    func presentVCs(_ vcname: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            UIView.animate(withDuration: 1.0) {
                self.gotoNavPresent(vcname, fullscreen: true, animated: false)
            }
        }
    }
    
    func setUI(_ index: ServiceType){
        self.lbl_intermedicate.text = "Tipo de\nlavado"
        if index == .book_service{
            self.setColor(true, view: self.uiv_book_service, imv: imv_bookservice, lbl: lbl_book_service)
            self.setColor(false, view: self.uiv_my_bookings, imv: imv_my_bookings, lbl: lbl_my_bookings)
            self.setColor(false, view: self.uiv_my_profile, imv: imv_my_profile, lbl: lbl_my_profile)
            self.setColor(false, view: self.uiv_settings, imv: imv_settings, lbl: lbl_settings)
            self.setColor(false, view: self.uiv_give_review, imv: imv_give_review, lbl: lbl_give_review)
        }else if index == .my_bookings{
            self.setColor(false, view: self.uiv_book_service, imv: imv_bookservice, lbl: lbl_book_service)
            self.setColor(true, view: self.uiv_my_bookings, imv: imv_my_bookings, lbl: lbl_my_bookings)
            self.setColor(false, view: self.uiv_my_profile, imv: imv_my_profile, lbl: lbl_my_profile)
            self.setColor(false, view: self.uiv_settings, imv: imv_settings, lbl: lbl_settings)
            self.setColor(false, view: self.uiv_give_review, imv: imv_give_review, lbl: lbl_give_review)
        }else if index == .my_profile{
            self.setColor(false, view: self.uiv_book_service, imv: imv_bookservice, lbl: lbl_book_service)
            self.setColor(false, view: self.uiv_my_bookings, imv: imv_my_bookings, lbl: lbl_my_bookings)
            self.setColor(true, view: self.uiv_my_profile, imv: imv_my_profile, lbl: lbl_my_profile)
            self.setColor(false, view: self.uiv_settings, imv: imv_settings, lbl: lbl_settings)
            self.setColor(false, view: self.uiv_give_review, imv: imv_give_review, lbl: lbl_give_review)
        }else if index == .settings{
            self.setColor(false, view: self.uiv_book_service, imv: imv_bookservice, lbl: lbl_book_service)
            self.setColor(false, view: self.uiv_my_bookings, imv: imv_my_bookings, lbl: lbl_my_bookings)
            self.setColor(false, view: self.uiv_my_profile, imv: imv_my_profile, lbl: lbl_my_profile)
            self.setColor(true, view: self.uiv_settings, imv: imv_settings, lbl: lbl_settings)
            self.setColor(false, view: self.uiv_give_review, imv: imv_give_review, lbl: lbl_give_review)
        }else if index == .give_review{
            self.setColor(false, view: self.uiv_book_service, imv: imv_bookservice, lbl: lbl_book_service)
            self.setColor(false, view: self.uiv_my_bookings, imv: imv_my_bookings, lbl: lbl_my_bookings)
            self.setColor(false, view: self.uiv_my_profile, imv: imv_my_profile, lbl: lbl_my_profile)
            self.setColor(false, view: self.uiv_settings, imv: imv_settings, lbl: lbl_settings)
            self.setColor(true, view: self.uiv_give_review, imv: imv_give_review, lbl: lbl_give_review)
        }else{
            self.setColor(false, view: self.uiv_book_service, imv: imv_bookservice, lbl: lbl_book_service)
            self.setColor(false, view: self.uiv_my_bookings, imv: imv_my_bookings, lbl: lbl_my_bookings)
            self.setColor(false, view: self.uiv_my_profile, imv: imv_my_profile, lbl: lbl_my_profile)
            self.setColor(false, view: self.uiv_settings, imv: imv_settings, lbl: lbl_settings)
            self.setColor(false, view: self.uiv_give_review, imv: imv_give_review, lbl: lbl_give_review)
        }
    }
    
    func setColor(_ selected: Bool, view: UIView, imv: UIImageView, lbl: UILabel) {
        if selected{
            view.backgroundColor = UIColor.init(named: "color_primary")
            imv.tintColor = .white
            lbl.textColor = .white
        }else{
            view.backgroundColor = UIColor.init(named: "color_edt_back")
            imv.tintColor = .darkGray
            lbl.textColor = .darkGray
        }
    }
}

enum VehicleType: Int {
    case sedan
    case hatchback
    case minivan
    case suv
    case camioneta
    case null
}

