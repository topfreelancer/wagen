//
//  SettingsVC.swift
//  Wagen
//
//  Created by top Dev on 10/24/20.
//

import UIKit

struct SettingOptions{
    static let settingOption = ["Términos y Condiciones","Política de Privacidad","Nuestro Servicio"]
}

class SettingsVC: BaseVC{

    var settingDatasource = [SettingModel]()
    var animator = CPImageViewerAnimator()
    @IBOutlet weak var tbv_settings: UITableView!
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.loadLayout()
        initDatasource()
        self.addleftButton()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func addleftButton() {
        let btn_back = UIButton(type: .custom)
        btn_back.setImage(UIImage (named: "ic_arrow_left")!.withRenderingMode(.alwaysTemplate), for: .normal)
        btn_back.addTarget(self, action: #selector(gotoHome), for: .touchUpInside)
        btn_back.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        btn_back.tintColor = UIColor.white
        
        let barButtonItemBack = UIBarButtonItem(customView: btn_back)
        barButtonItemBack.customView?.widthAnchor.constraint(equalToConstant: 20).isActive = true
        barButtonItemBack.customView?.heightAnchor.constraint(equalToConstant: 25).isActive = true
        self.navigationItem.leftBarButtonItem = barButtonItemBack
    }
    
    @objc func gotoHome() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func logout(_ sender: Any) {
        thisuser!.clearUserInfo()
        UserDefault.setBool(key: PARAMS.IS_CHEKCED, value: false)
        self.gotoVC(VCs.LOGIN_NAV)
    }
    
    func loadLayout() {
        self.title = "Configuración"
    }
    
    func initDatasource() {
        self.settingDatasource.removeAll()
        var num = 0
        for i in 0 ... 2{
            num += 1
            self.settingDatasource.append(SettingModel(SettingOptions.settingOption[i]))
            if num == 3{
                self.tbv_settings.reloadData()
            }
        }
    }
}

extension SettingsVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return settingDatasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingCell") as! SettingCell
        cell.entity = settingDatasource[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let aa = indexPath.row
        switch  aa {
        
        case 0 :// terms
            self.gotoWebViewWithProgressBar(weburls.terms_url ?? "")
            break
        case 1 : // privacy
            self.gotoWebViewWithProgressBar(weburls.privacy_url ?? "")
            break
        case 2 : //about us
            /**let controller = CPImageViewer()
            controller.image = UIImage.init(named: "about_service")
            self.navigationController?.delegate = self.animator
            self.present(controller, animated: true, completion: nil)*/
            self.gotoWebViewWithProgressBar(weburls.aboutservice_url ?? "")
            break
        default :
            print(aa)
        }
        tableView.reloadData()
    }
}


