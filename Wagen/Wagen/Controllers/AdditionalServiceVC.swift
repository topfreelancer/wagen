//
//  AdditionalServiceVC.swift
//  Wagen
//
//  Created by top Dev on 10/25/20.
//

import UIKit
import SwiftyJSON
import GMStepper

class AdditionalServiceVC: BaseVC{
    
    @IBOutlet weak var tbv_additional_services: UITableView!
    @IBOutlet weak var lbl_intermediate: UILabel!
    var ds_additional_services = [AdditionalServiceModel]()
    let cellSpacingHeight: CGFloat = 20
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        getDataSource()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if self.hud != nil{
            if hud!.isFocused{
                self.hideLoadingView()
            }
        }
    }
    
    func setUI() {
        self.lbl_intermediate.text = "Tipo de\nlavado"
        self.addleftButton()
        self.title = "Servicios adicionales"
        tbv_additional_services.rowHeight = UITableView.automaticDimension
        tbv_additional_services.estimatedRowHeight = 80
        tbv_additional_services.tableFooterView = UIView()
    }
    
    func addleftButton() {
        let btn_back = UIButton(type: .custom)
        btn_back.setImage(UIImage (named: "ic_arrow_left")!.withRenderingMode(.alwaysTemplate), for: .normal)
        btn_back.addTarget(self, action: #selector(gotoHome), for: .touchUpInside)
        btn_back.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        btn_back.tintColor = UIColor.white
        
        let barButtonItemBack = UIBarButtonItem(customView: btn_back)
        barButtonItemBack.customView?.widthAnchor.constraint(equalToConstant: 20).isActive = true
        barButtonItemBack.customView?.heightAnchor.constraint(equalToConstant: 25).isActive = true
        self.navigationItem.leftBarButtonItem = barButtonItemBack
    }
    
    @objc func gotoHome() {
        self.navigationController?.popViewController(animated: false)
    }
    
    func getDataSource()  {
        self.ds_additional_services.removeAll()
        var num = 0
        for i in 0 ... services.count - 1{
            num += 1
            let one = services[i]
            self.ds_additional_services.append(AdditionalServiceModel(id: one.service_id ?? "", additional_service_type: one.service_name ?? "", price: one.service_price ?? "", duration: one.service_time ?? "", selected: false,cus_status: one.cu_status ?? "0", countt: 0))
            if num == services.count{
                self.tbv_additional_services.reloadData()
            }
        }
    }
    
    @IBAction func selectBtnClicked(_ sender: Any) {
        let button = sender as! UIButton
        let index = button.tag
        self.ds_additional_services[index].selected = !self.ds_additional_services[index].selected
        
        let indexset = IndexSet.init(integer: index)
        self.tbv_additional_services.reloadSections(indexset, with: .automatic)
    }
    
    @IBAction func nextBtnClicked(_ sender: Any) {
        // here check prices and time duration
        var select_price = 0
        var select_duration = 0
        var count_price = 0
        var count_duration = 0
        var service_ids = ""
        var service_qty = ""
        var num = 0
        for one in self.ds_additional_services{
            num += 1
            if one.selected{
                select_price += one.price?.toInt() ?? 0
                select_duration += one.duration?.toInt() ?? 0
                if service_ids.isEmpty{
                    service_ids += one.id!
                    service_qty += "1"
                }else{
                    service_ids = service_ids + "," + one.id!
                    service_qty = service_qty + "," + "1"
                }
            }else{
                if one.cus_status == "1"{
                    if one.countt != 0{
                        count_price += one.countt * (one.price?.toInt()!)!
                        count_duration += one.countt * (one.duration?.toInt()!)!
                        if service_ids.isEmpty{
                            service_ids += one.id!
                            service_qty += "\(one.countt)"
                        }else{
                            service_ids = service_ids + "," +  one.id!
                            service_qty = service_qty + "," + "\(one.countt)"
                        }
                    }
                }
            }
        }
        additional_service_summary = SummaryModel()
        additional_service_summary.price = "\(select_price + count_price)"
        additional_service_summary.duration = "\(select_duration + count_duration)"
        additional_service_summary.service_ids = service_ids
        additional_service_summary.service_qty = service_qty
        print(main_service_summary.price, main_service_summary.duration, main_service_summary.package_id)
        print(additional_service_summary.price, additional_service_summary.duration, additional_service_summary.service_ids, additional_service_summary.service_qty)
        
        let tovc = self.createVC(VCs.CALENDAR) as! CalendarVC
        tovc.order_type = main_service_summary.order_type ?? OrderType.workshop
        if main_service_summary.order_type == OrderType.homeorder{
            tovc.selected_city = main_service_summary.order_city
        }else{
            tovc.selected_city = main_service_summary.address
        }
        //self.gotoNavPresent(VCs.CALENDAR, fullscreen:true, animated: false)
        self.gotoNavPresentWithVC(tovc, fullscreen: true)
    }
    
    @IBAction func valuechanged(_ sender: Any) {
        let stepper = sender as! GMStepper
        let index = stepper.tag
        self.ds_additional_services[index].countt = Int(stepper.value)
    }
}

extension AdditionalServiceVC : UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.ds_additional_services.count
    }
    
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tbv_additional_services?.dequeueReusableCell(withIdentifier: "AdditionalServiceCell", for:indexPath) as! AdditionalServiceCell
        cell.selectionStyle = .none
        cell.entity = ds_additional_services[indexPath.section]
        cell.btn_select.tag = indexPath.section
        cell.cus_steppter.tag = indexPath.section
        cell.cus_steppter.value = Double(ds_additional_services[indexPath.section].countt)
        if ds_additional_services[indexPath.section].cus_status == "1"{
            cell.btn_select.isHidden = true
            cell.cus_steppter.isHidden = false
            cell.uiv_outline.isHidden = false
        }else{
            cell.btn_select.isHidden = false
            cell.cus_steppter.isHidden = true
            cell.uiv_outline.isHidden = true
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return cellSpacingHeight
    }
}


