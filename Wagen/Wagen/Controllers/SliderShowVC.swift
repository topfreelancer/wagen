//
//  SliderShowVC.swift
//  Wagen
//
//  Created by top Dev on 28.11.2020.
//

import UIKit
import ImageSlideshow

class SliderShowVC: BaseVC {

    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_content : UILabel!
    @IBOutlet weak var slideshow: ImageSlideshow!
    var imageData = [InputSource]()
    var promo_title = promotions.first?.title
    var promo_content = promotions.first?.descriptionn
    
    override func viewDidLoad() {
        super.viewDidLoad()
        slideShowInit()
        self.title = "Acerca de nosotros"
        self.setTitle_Content()
        self.addleftButton()
    }
    
    func addleftButton() {
        let btn_back = UIButton(type: .custom)
        btn_back.setImage(UIImage (named: "ic_arrow_left")!.withRenderingMode(.alwaysTemplate), for: .normal)
        btn_back.addTarget(self, action: #selector(gotoHome), for: .touchUpInside)
        btn_back.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        btn_back.tintColor = UIColor.white
        
        let barButtonItemBack = UIBarButtonItem(customView: btn_back)
        barButtonItemBack.customView?.widthAnchor.constraint(equalToConstant: 20).isActive = true
        barButtonItemBack.customView?.heightAnchor.constraint(equalToConstant: 25).isActive = true
        self.navigationItem.leftBarButtonItem = barButtonItemBack
    }
    
    @objc func gotoHome() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func slideShowInit()  {
        slideshow.slideshowInterval = 0.0
        slideshow.pageIndicatorPosition = .init(horizontal: .center, vertical: .under)
        slideshow.contentScaleMode = UIViewContentMode.scaleAspectFit
        setIndicator(slideshow, "banner")
        if #available(iOS 13.0, *) {
            slideshow.activityIndicator = DefaultActivityIndicator(style: .medium, color: nil)
        } else {
            // Fallback on earlier versions
        }
        slideshow.delegate = self
        self.imageData.removeAll()
        for one in promotions{
            if let url = one.full_image{
                if let image = KingfisherSource(urlString: url){
                    self.imageData.append(image)
                }
            }
        }
        slideshow.setImageInputs(imageData)
    }
    
    func setTitle_Content()  {
        self.lbl_title.text = self.promo_title
        self.lbl_content.text = self.promo_content
    }
    
    func setIndicator (_ imgSlider: ImageSlideshow, _ type: String) {
        let pageControl = UIPageControl()
        pageControl.currentPageIndicatorTintColor = UIColor.init(named: "color_primary")
        pageControl.pageIndicatorTintColor = .lightGray
        imgSlider.pageIndicator = pageControl
        imgSlider.pageIndicatorPosition = PageIndicatorPosition(horizontal: .center, vertical: .bottom)
    }
}


extension SliderShowVC: ImageSlideshowDelegate{
    func imageSlideshow(_ imageSlideshow: ImageSlideshow, didChangeCurrentPageTo page: Int) {
        if page <= promotions.count - 1 {
            if let title = promotions[page].title, let content = promotions[page].descriptionn{
                self.promo_title = title
                self.promo_content = content
            }
        }
        self.setTitle_Content()
    }
}
