//
//  GiveReveiwVC.swift
//  Wagen
//
//  Created by top Dev on 11/4/20.
//

import UIKit
import SwiftyJSON

class GiveReveiwVC: BaseVC{

    @IBOutlet weak var tbv_my_bookings: UITableView!
    var ds_mybookings = [ReviewModel]()
    var isRequesting = false
    let cellSpacingHeight: CGFloat = 20
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //toVC?.animatedItems[3].badgeValue = nil
        //badgeVal = nil
        //self.tabBarController?.delegate = self
        //toVC?.animatedItems[3].badgeValue = nil
        setUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.isRequesting = false
        getDataSource()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if self.hud != nil{
            if hud!.isFocused{
                self.hideLoadingView()
            }
        }
    }
    
    func setUI() {
        self.addleftButton()
        self.title = "Lavados pendientes de revisión"
        tbv_my_bookings.tableFooterView = UIView()
    }
    
    func addleftButton() {
        let btn_back = UIButton(type: .custom)
        btn_back.setImage(UIImage (named: "ic_arrow_left")!.withRenderingMode(.alwaysTemplate), for: .normal)
        btn_back.addTarget(self, action: #selector(gotoHome), for: .touchUpInside)
        btn_back.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        btn_back.tintColor = UIColor.white
        
        let barButtonItemBack = UIBarButtonItem(customView: btn_back)
        barButtonItemBack.customView?.widthAnchor.constraint(equalToConstant: 20).isActive = true
        barButtonItemBack.customView?.heightAnchor.constraint(equalToConstant: 25).isActive = true
        self.navigationItem.leftBarButtonItem = barButtonItemBack
    }
    
    @objc func gotoHome() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func getDataSource()  {
        self.showLoadingView(vc: self)
        ApiManager.getallMyReviewList { (isSuccess, data) in
            self.hideLoadingView()
            if isSuccess{
                let orderinfo = JSON(data as Any)
                if let orders = orderinfo["orders"].arrayObject{
                    if orders.count != 0{
                        self.ds_mybookings.removeAll()
                        var num = 0
                        for one in orders{
                            let jsonone = JSON(one as Any)
                            num += 1
                            var num1 = 0
                            var services = [ServicesModel]()
                            if let servicess = jsonone["selections"].arrayObject{
                                if servicess.count != 0{
                                    services.removeAll()
                                    for one in servicess{
                                        num1 += 1
                                        services.append(ServicesModel(JSON(one as Any)))
                                        if num1 == servicess.count{
                                            self.ds_mybookings.append(ReviewModel(id: jsonone["order_id"].stringValue, date: jsonone["order_date"].stringValue, time: jsonone["order_time"].stringValue, duration: jsonone["duration_time"].stringValue, price: jsonone["total_price"].stringValue, car_id: jsonone["car_id"].stringValue, car_name: jsonone["car_name"].stringValue, car_photo: jsonone["car_photo"].stringValue, package_id: jsonone["package_id"].stringValue, order_type: jsonone["order_type"].stringValue, services: services))
                                            if num == orders.count{
                                                self.tbv_my_bookings.reloadData()
                                            }
                                        }
                                    }
                                }else{
                                    self.ds_mybookings.append(ReviewModel(id: jsonone["order_id"].stringValue, date: jsonone["order_date"].stringValue, time: jsonone["order_time"].stringValue, duration: jsonone["duration_time"].stringValue, price: jsonone["total_price"].stringValue, car_id: jsonone["car_id"].stringValue, car_name: jsonone["car_name"].stringValue, car_photo: jsonone["car_photo"].stringValue, package_id: jsonone["package_id"].stringValue, order_type: jsonone["order_type"].stringValue, services: services))
                                    if num == orders.count{
                                        self.tbv_my_bookings.reloadData()
                                    }
                                }
                            }
                        }
                    }else{
                        self.showToast("no hay ninguna lista de pedidos.")
                    }
                }else{
                    self.showToast("no hay ninguna lista de pedidos.")
                }
            }
        }
    }
}

extension GiveReveiwVC : UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.ds_mybookings.count
    }
    
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tbv_my_bookings?.dequeueReusableCell(withIdentifier: "MyBookingCell", for:indexPath) as! MyBookingCell
        cell.selectionStyle = .none
        cell.setReview(ds_mybookings[indexPath.section])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //return Constants.SCREEN_HEIGHT / 10
        return 150
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let tovc = self.createVC(VCs.WRITE_REVIEW) as! WriteReviewVC
        tovc.selected_review = self.ds_mybookings[indexPath.section]
        //self.gotoNavPresent(VCs.WRITE_REVIEW, fullscreen: true, animated: false)
        self.gotoNavPresentWithVC(tovc, fullscreen: true)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return cellSpacingHeight
    }
}


