//
//  BookingSummaryVC.swift
//  Wagen
//
//  Created by top Dev on 10/25/20.
//

import UIKit
import MercadoPagoSDK
import SwiftyJSON

class BookingSummaryVC: BaseVC {
    
    @IBOutlet weak var lbl_location: UILabel!
    @IBOutlet weak var lbl_date: UILabel!
    @IBOutlet weak var lbl_time: UILabel!
    @IBOutlet weak var lbl_duration: UILabel!
    @IBOutlet weak var lbl_total_price: UILabel!
    @IBOutlet weak var cons_top_btnsave: NSLayoutConstraint!
    @IBOutlet weak var edt_couponcode: UITextField!
    @IBOutlet weak var uiv_dlgback: UIView!
    @IBOutlet weak var uiv_dlg: UIView!
    var address = ""
    var datee = ""
    var timee = ""
    var duration = ""
    var price = ""
    var car_id = ""
    var order_type = ""
    var package_id = ""
    var service_ids = ""
    var service_qty = ""
    var coupon_id = "-1"
    var discount_percent = "0"
    var paid_amount = ""
    
    var order_model:OrderModel?
    
    private var checkout: MercadoPagoCheckout?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setPaymentType(false)
    }
    
    func setPaymentType(_ is_show: Bool)  {
        if is_show{
            self.uiv_dlgback.isHidden =  false
            self.uiv_dlg.isHidden = false
        }else{
            self.uiv_dlgback.isHidden =  true
            self.uiv_dlg.isHidden = true
        }
    }
    
    func setUI() {
        self.cons_top_btnsave.constant = -4
        self.edt_couponcode.isHidden = true
        self.title = "Resumen de Reserva"
        address = main_service_summary.address ?? ""
        datee = main_service_summary.datee ?? ""
        timee = main_service_summary.timee ?? ""
        duration = "\((main_service_summary.duration?.toInt())! + (additional_service_summary.duration?.toInt())!)"
        price = "\((main_service_summary.price?.toInt())! + (additional_service_summary.price?.toInt())!)"
        paid_amount = price
        car_id = main_service_summary.car_id ?? "1"
        order_type = main_service_summary.order_type ?? OrderType.workshop
        service_ids = additional_service_summary.service_ids ?? ""
        service_qty = additional_service_summary.service_qty ?? ""
        package_id = main_service_summary.package_id ?? ""
        self.lbl_location.text = address
        self.lbl_date.text = "Fecha de Reserva \n" + datee
        self.lbl_time.text = "Hora de tu Reserva \n" + timee
        self.lbl_duration.text = "Duración \n" + duration + "MINS"
        self.lbl_total_price.text = "Precio Total \n" + splitWithDot(price) + "CLP"
        self.addleftButton()
    }
    
    func addleftButton() {
        let btn_back = UIButton(type: .custom)
        btn_back.setImage(UIImage (named: "ic_arrow_left")!.withRenderingMode(.alwaysTemplate), for: .normal)
        btn_back.addTarget(self, action: #selector(gotoHome), for: .touchUpInside)
        btn_back.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        btn_back.tintColor = UIColor.white
        
        let barButtonItemBack = UIBarButtonItem(customView: btn_back)
        barButtonItemBack.customView?.widthAnchor.constraint(equalToConstant: 20).isActive = true
        barButtonItemBack.customView?.heightAnchor.constraint(equalToConstant: 25).isActive = true
        self.navigationItem.leftBarButtonItem = barButtonItemBack
    }
    
    @objc func gotoHome() {
        //self.navigationController?.popViewController(animated: true)
        self.gotoNavPresent(VCs.SELECT_VEHICLE, fullscreen: true, animated: true)
    }
    
    @IBAction func orderBtnClicked(_ sender: Any) {
        // show alert
        self.setPaymentType(true)
    }
    
    @IBAction func couponBtnclicked(_ sender: Any) {
        self.edt_couponcode.isHidden = false
        self.setEdtPlaceholder(self.edt_couponcode, placeholderText: "Please enter coupon code", placeColor: .black, padding: .left(10))
        self.cons_top_btnsave.constant = 40
        self.edt_couponcode.becomeFirstResponder()
    }
    
    @IBAction func byMembershipBtnClicked(_ sender: Any) {
        self.setPaymentType(false)
        // check coupon code
        if let couponcode = self.edt_couponcode.text{
            if !couponcode.isEmpty{
                self.checkCouponCode(couponcode)
                return
            }
        }
        // checking membership payment count if okay, request create order api
        if thisuser!.membership_count!.toInt() ?? 0 > 0{
            self.showLoadingView(vc: self)
            ApiManager.createorder(order_date: datee, order_time: timee, duration_time: duration, total_price: price, car_id: car_id, package_id: package_id, service_ids: service_ids, order_type: order_type, order_city: order_type == OrderType.homeorder ? main_service_summary.order_city ?? "" : "", order_address: order_type == OrderType.homeorder ? main_service_summary.address ?? "" :"", payment_method: PaymentType.membership, service_qty: service_qty, coupon_id: coupon_id, discount_percent: discount_percent, paid_price: paid_amount) { (isSuccess, data) in
                self.hideLoadingView()
                if isSuccess{
                    // reduce user's membership count
                    let membership_count = thisuser?.membership_count?.toInt()
                    let newmembershipcount = membership_count! - 1
                    //save user information again
                    UserDefault.setString(key: "membership_count", value: "\(newmembershipcount)")
                    thisuser!.loadUserInfo()
                    thisuser!.saveUserInfo()
                    self.showCustomDialog(titlee: "Message", content: "Gracias por tu reserva y confiar\nen nosotros")
                }else{
                    self.showToast(Messages.NETISSUE)
                }
            }
        }else{
            self.showToast(Messages.PURCHASE_MEMBERSHIP)
        }
    }
    
    @IBAction func payNowBtnClickedd(_ sender: Any) {
        pay_type = .direct_pay
        // check coupon code
        if let couponcode = self.edt_couponcode.text{
            if !couponcode.isEmpty{
                self.checkCouponCode(couponcode)
                self.setPaymentType(false)
                return
            }
        }
        
        order_model = OrderModel(order_date: datee, order_time: timee, duration_time: duration, total_price: price, car_id: car_id, package_id: package_id, service_ids: service_ids, order_type: order_type, order_city: order_type == OrderType.homeorder ? main_service_summary.order_city ?? "" : "", order_address: order_type == OrderType.homeorder ? main_service_summary.address ?? "" :"", payment_method: PaymentType.directpay, service_qty: service_qty, coupon_id: coupon_id, discount_percent: discount_percent, paid_price: paid_amount)
        self.setPaymentType(false)
        self.showLoadingView(vc: self)
        ApiManager.getPreferenceId(amount: price) { (isSuccess, data) in
            self.hideLoadingView()
            if isSuccess{
                let dict = JSON(data as Any)
                //****************** local payment ***************//
                let builder = MercadoPagoCheckoutBuilder(publicKey: Constants.MercadoPublicKey, preferenceId:dict["preferenceid"].stringValue).setLanguage("es")
                self.checkout = MercadoPagoCheckout(builder: builder)
                // Start with your navigation controller.
                if let myNavigationController = self.navigationController {
                    self.checkout?.start(navigationController: myNavigationController, lifeCycleProtocol: self)
                }
            }
        }
    }
    
    func checkCouponCode(_ coupon_id: String) {
        for one in couponcodes{
            if one.code == coupon_id{
                self.coupon_id = one.code ?? "-1"
                self.discount_percent = one.discount_value ?? "0"
                if let discountd = self.discount_percent.toInt(){
                    let percent = 100 - discountd
                    self.paid_amount = "\(self.price.toInt()! * percent / 100)"
                }
            }else{
                self.showToast(Messages.INPUT_CORRECT_COUPON_CODE)
            }
        }
    }
}

struct PaymentType {
    public static let membership = "0"
    public static let directpay = "1"
}

struct OrderType {
    public static let workshop = "0"
    public static let homeorder = "1"
}

extension BookingSummaryVC :PXLifeCycleProtocol{
    func cancelCheckout() -> (() -> Void)? {
        return {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func finishCheckout() -> ((PXResult?) -> Void)? {
        return  ({ (_ payment: PXResult?) in
            print(payment)
            if let payment  = payment{
                if payment.getStatus() == "approved"{
                    if let order = self.order_model{
                        ApiManager.createorder(order_date: order.order_date ?? "", order_time: order.order_time ?? "", duration_time: order.duration_time ?? "", total_price: order.total_price ?? "", car_id: order.car_id ?? "", package_id: order.package_id ?? "", service_ids: order.service_ids ?? "", order_type: order.order_type ?? "", order_city: order.order_city ?? "", order_address: order.order_address ?? "", payment_method: order.payment_method ?? "", service_qty: order.service_qty ?? "", coupon_id: order.coupon_id ?? "", discount_percent: order.discount_percent ?? "", paid_price: order.paid_price ?? "") { (isSuccess, data) in
                            if isSuccess{
                                self.showCustomDialog(titlee: "Message", content: "Gracias por tu reserva y confiar\nen nosotros")
                            }else{
                                self.showAlerMessage(message: Messages.NETISSUE)
                            }
                        }
                    }
                }
            }else{
                self.navigationController?.popViewController(animated: true)
            }
            //self.navigationController?.popToRootViewController(animated: false)
        })
    }
}
