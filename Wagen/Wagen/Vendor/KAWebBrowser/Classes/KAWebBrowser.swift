//
//  KAWebBrowser.swift
//  KAWebBrowser
// 
//  Created by zhihuazhang on 2016/06/08.
//  Copyright © 2016年 Kapps Inc. All rights reserved.
//

import UIKit
import WebKit

private let KVOEstimatedProgress = "estimatedProgress"
private let KVOTitle = "title"

open class KAWebBrowser: UIViewController , WKNavigationDelegate{

    fileprivate lazy var webView: WKWebView = {
        let webView = WKWebView(frame: CGRect.zero)
        return webView
    }()
    
    fileprivate lazy var progressBar: UIProgressView = {
        let progressBar = UIProgressView()
        progressBar.tintColor = UIColor.init(named: "color_primary")
        progressBar.alpha = 0
        return progressBar
    }()
    
    open class func navigationControllerWithBrowser() -> UINavigationController {
        let browser = KAWebBrowser()
        
        let closeBtn = UIBarButtonItem(barButtonSystemItem: .stop, target: browser, action: #selector(closeButtonPressed))
        browser.navigationItem.leftBarButtonItem = closeBtn

        return UINavigationController(rootViewController: browser)
    }
    
    open func loadURLString(_ urlString: String) {
        guard let url = URL(string: urlString) else {
            return
        }
        
        print(url.absoluteString)
        loadURL(url)
    }
    
    open func loadURL(_ url: URL) {
        let request = URLRequest(url: url)
        loadRequest(request)
    }
    
    open func loadRequest(_ request: URLRequest) {
        webView.load(request)
    }
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(webView)
        webView.translatesAutoresizingMaskIntoConstraints = false
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[webView]|", options: .alignAllLeft, metrics: nil, views: ["webView": webView]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[webView]|", options: .alignAllTop, metrics: nil, views: ["webView": webView]))
        
        webView.addObserver(self, forKeyPath: KVOTitle, options: .new, context: nil)
        
        guard let _ = navigationController else {
            print("need UINavigationController to show progressbar")
            return
        }
        
        webView.addObserver(self, forKeyPath: "estimatedProgress", options: .new, context: nil)
        
        view.addSubview(progressBar)
        progressBar.translatesAutoresizingMaskIntoConstraints = false
        view.addConstraint(NSLayoutConstraint(item: progressBar, attribute: .top, relatedBy: .equal, toItem: topLayoutGuide, attribute: .bottom, multiplier: 1, constant: 0))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[progressBar]|", options: .alignAllLeft, metrics: nil, views: ["progressBar": progressBar]))
        webView.navigationDelegate = self
        addleftButton()
    }
    
    public func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        print("didFail: \(error)")
    }
    
    public func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print("didFailProvisionalNavigation: \(error)")
    }
    
    public func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        if webView.url!.absoluteString.range(of: "Paymentend") != nil{
            let scriptSource = "javascript:sendmessage();"
            webView.evaluateJavaScript(scriptSource, completionHandler: { (object, error) in
                if let message = object as? Int{
                    if message == 0{
                        if pay_type == .direct_pay{
                            /**if let order = order_model{
                                ApiManager.createorder(order_date: order.order_date ?? "", order_time: order.order_time ?? "", duration_time: order.duration_time ?? "", total_price: order.total_price ?? "", car_id: order.car_id ?? "", package_id: order.package_id ?? "", service_ids: order.service_ids ?? "", order_type: order.order_type ?? "", order_city: order.order_city ?? "", order_address: order.order_address ?? "", payment_method: order.payment_method ?? "", service_qty: order.service_qty ?? "", coupon_id: order.coupon_id ?? "", discount_percent: order.discount_percent ?? "", paid_price: order.paid_price ?? "") { (isSuccess, data) in
                                    if isSuccess{
                                        self.showCustomDialog(content: "Gracias por tu reserva y confiar\nen nosotros")
                                    }else{
                                        self.showAlerMessage(message: Messages.NETISSUE)
                                    }
                                }
                            }*/
                            print("direcet pay")
                        }else if pay_type == .membership_pay{
                            if let membership = paidmembership_model{
                                ApiManager.purchasemembership(membershipid: membership.id ?? "1", memership_count: membership.max_order_per_month ?? "") { (isSuccess1, renewdate, remaining) in
                                    if isSuccess1{
                                        UserDefault.setString(key: "membership_id", value: membership.id ?? "1")
                                        UserDefault.setString(key: "membership_last_renew", value: renewdate)
                                        UserDefault.setString(key: "membership_count", value: remaining)
                                        UserDefault.Sync()
                                        thisuser!.loadUserInfo()
                                        thisuser!.saveUserInfo()
                                        self.navigationController?.popViewController(animated: true)
                                    }else{
                                        self.showAlerMessage(message: Messages.NETISSUE)
                                    }
                                }
                            }
                        }
                    }else{
                        let alertController = UIAlertController(title: nil, message: "payment fail", preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            self.navigationController?.popViewController(animated: true)
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                }
            })
        }
    }
    
    func showCustomDialog(content: String) {
        let ratingVC = RatingViewController(nibName: "RatingViewController", bundle: nil)
        ratingVC.content = content
        ratingVC.modalPresentationStyle = .overFullScreen
        self.present(ratingVC, animated: false, completion: nil)
    }
    
    func addleftButton() {
        let btn_back = UIButton(type: .custom)
        btn_back.setImage(UIImage (named: "ic_arrow_left")!.withRenderingMode(.alwaysTemplate), for: .normal)
        btn_back.addTarget(self, action: #selector(gotoHome), for: .touchUpInside)
        btn_back.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        btn_back.tintColor = UIColor.white
        let barButtonItemBack = UIBarButtonItem(customView: btn_back)
        barButtonItemBack.customView?.widthAnchor.constraint(equalToConstant: 20).isActive = true
        barButtonItemBack.customView?.heightAnchor.constraint(equalToConstant: 25).isActive = true
        self.navigationItem.leftBarButtonItem = barButtonItemBack
    }
    
    @objc func gotoHome() {
        self.navigationController?.popViewController(animated: true)
        //self.gotoVC(VCs.HOME_NAV)
    }

    fileprivate func updateProgressBar(_ progress: Float) {
        if progress == 1.0 {
            progressBar.setProgress(progress, animated: true)
            UIView.animate(withDuration: 1.5, animations: {
                self.progressBar.alpha = 0.0
                }, completion: { finished in
                    if finished {
                        self.progressBar.setProgress(0.0, animated: false)
                    }
            })
        } else {
            if progressBar.alpha < 1.0 {
                progressBar.alpha = 1.0
            }
            progressBar.setProgress(progress, animated: (progress > progressBar.progress) && true)
        }
    }
    
    @objc func closeButtonPressed() {
        navigationController?.dismiss(animated: true, completion: nil)
    }
    
    deinit{
        webView.removeObserver(self, forKeyPath: KVOEstimatedProgress)
        webView.removeObserver(self, forKeyPath: KVOTitle)
    }

    // MARK: - KVO
    
    override open func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == KVOEstimatedProgress {
            updateProgressBar(Float(webView.estimatedProgress))
        }
        
        if keyPath == KVOTitle {
            //title = self.webView.title
        }
    }

}

extension UINavigationController {
    
    public func webBrowser() -> KAWebBrowser? {
        return viewControllers.first as? KAWebBrowser
    }
}

extension KAWebBrowser : WKScriptMessageHandler {
    public func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        print(message.body)
        let dict = message.body as? Dictionary<String, String>
        print(dict)
        //let userdata = UserData((dict?["firstName"])!, (dict?["email"])!, (dict?["lastName"])!)
        /*if message.name == "sumbitToiOS" {
            self.sumbitToiOS(user: userdata)
        }*/
    }
}
