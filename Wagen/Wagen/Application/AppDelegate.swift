//
//  AppDelegate.swift
//  Wagen
//
//  Created by top Dev on 10/23/20.
//

import UIKit
import IQKeyboardManagerSwift
import FBSDKCoreKit
import GoogleSignIn

var thisuser:UserModel?
@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        IQKeyboardManager.shared.enable = true
        // user setting
        thisuser = UserModel()
        thisuser?.loadUserInfo()
        thisuser?.saveUserInfo()
        // for google signin
        GIDSignIn.sharedInstance().clientID = Constants.CLIENT_ID
        return true
    }

    // MARK: UISceneSession Lifecycle

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    func application(_ application: UIApplication,
                     continue userActivity: NSUserActivity,
                     restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
      
      // 1
      guard userActivity.activityType == NSUserActivityTypeBrowsingWeb,
        let url = userActivity.webpageURL,
        let components = URLComponents(url: url, resolvingAgainstBaseURL: true) else {
          return false
      }
      
      // 2
//      if let computer = ItemHandler.sharedInstance.items.filter({ $0.path == components.path}).first {
//        presentDetailViewController(computer)
//        return true
//      }
        print(components)
      
      // 3
      if let webpageUrl = URL(string: "https://wagen.cl/admin/index.php/Paymentend") {
        application.open(webpageUrl)
        return false
      }
      
      return false
    }


}

